<?php $ci =& get_instance();
$current_url = uri_string();
$user_data = $ci->session->userdata('user_data');
$menus = array(
    'admin' => array(
        'icon' => 'fa fa-dashboard',
        'title' => 'Dashboard'
    )
);

$menus['admin/category?type=1'] = array(
    'icon' => 'glyphicon glyphicon-signal',
    'title' => 'Building'
);
$menus['admin/category?type=2'] = array(
    'icon' => 'glyphicon glyphicon-th',
    'title' => 'Tenants'
);
$menus['admin/prices'] = array(
    'icon' => 'glyphicon glyphicon-usd',
    'title' => 'Invoices'
);
if($user_data->role==1){
    $menus['user'] = array(
        'icon' => 'glyphicon glyphicon-user',
        'title' => 'Accounts',
        'sub' => array(
            'user/add' => 'Add',
            'user' => 'List'
        )
    );
}
$menus['auth/setting'] = array(
    'icon' => ' glyphicon glyphicon-cog',
    'title' => 'Setting'
);
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu" style="margin-top:8px;">
            <li class="header">Main menu</li>
            <?php foreach($menus as $path=>$item){
                $url = base_url($path);
                $have_sub = isset($item['sub']) && !empty($item['sub']);
                $segment_2 = $this->uri->segment(2);
                $segments = $this->uri->segment(1).($segment_2 ? '/'.$segment_2 : '');
                $is_active = $segments==$path;
                if($have_sub){
                    if( isset($item['sub'][$current_url]) || isset($item['sub'][$segments]) ){
                        $is_active = true;
                    }
                }
                ?>
                <li class="<?php if($is_active){echo 'active ';}?>treeview">
                    <a href="<?php echo $url;?>">
                        <i class="<?php echo $item['icon']?>"></i>
                        <span><?php echo $item['title'];?></span>
                        <?php if($have_sub){?>
                            <i class="fa fa-angle-left pull-right"></i>
                        <?php }?>
                    </a>
                    <?php if($have_sub){?>
                        <ul class="treeview-menu">
                            <?php foreach($item['sub'] as $sub_path=>$sub){?>
                                <li class="<?php if(base_url($sub_path)==$current_url){echo 'active';}?>">
                                    <a href="<?php echo base_url($sub_path);?>"><?php echo $sub;?></a>
                                </li>
                            <?php }?>
                        </ul>
                    <?php }?>
                </li>
            <?php }?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
			
			