<?php $this->load->view('header') ?>
<?php $this->load->view('sidebar'); ?>
    <div class="content-wrapper" style="min-height: 946px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $page_title;?></h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php $this->load->view($template); ?>
        </section><!-- /.content -->
    </div>

<?php $this->load->view('footer') ?>