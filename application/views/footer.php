<?php $asset = base_url('public/admin/');
?><footer class="main-footer">
<div class="pull-right hidden-xs">
  <b>Version</b> 1.0
</div>
<strong>Copyright &copy; 2017 Building Manager</strong>
</footer>
        <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
	<script>
      //$.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo $asset.'bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo $asset.'plugins/tinymce/tinymce.min.js' ?>"></script>
    <!-- iCheck 1.0.1 -->
    <script src="<?php echo $asset.'plugins/iCheck/icheck.min.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $asset.'dist/js/app.min.js' ?>"></script>

    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo $asset.'dist/js/demo.js' ?>"></script>
  </body>
</html>