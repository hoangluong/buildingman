<?php
if(!function_exists('url_friendly')) {
    function url_friendly($string, $space="-"){
        return sanitizeTitle($string, $space);
    }
}
if(!function_exists('sanitizeTitle')) {
    function sanitizeTitle($string, $space="-") {
        if(!$string) return false;
        $unicode = array(
            '0'    => array('°', '₀', '۰'),
            '1'    => array('¹', '₁', '۱'),
            '2'    => array('²', '₂', '۲'),
            '3'    => array('³', '₃', '۳'),
            '4'    => array('⁴', '₄', '۴', '٤'),
            '5'    => array('⁵', '₅', '۵', '٥'),
            '6'    => array('⁶', '₆', '۶', '٦'),
            '7'    => array('⁷', '₇', '۷'),
            '8'    => array('⁸', '₈', '۸'),
            '9'    => array('⁹', '₉', '۹'),
            'a'    => array('à', 'á', 'ả', 'ã', 'ạ', 'ă', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ', 'â', 'ấ', 'ầ', 'ẩ', 'ẫ', 'ậ', 'ā', 'ą', 'å', 'α', 'ά', 'ἀ', 'ἁ', 'ἂ', 'ἃ', 'ἄ', 'ἅ', 'ἆ', 'ἇ', 'ᾀ', 'ᾁ', 'ᾂ', 'ᾃ', 'ᾄ', 'ᾅ', 'ᾆ', 'ᾇ', 'ὰ', 'ά', 'ᾰ', 'ᾱ', 'ᾲ', 'ᾳ', 'ᾴ', 'ᾶ', 'ᾷ', 'а', 'أ', 'အ', 'ာ', 'ါ', 'ǻ', 'ǎ', 'ª', 'ა', 'अ', 'ا'),
            'b'    => array('б', 'β', 'Ъ', 'Ь', 'ب', 'ဗ', 'ბ'),
            'c'    => array('ç', 'ć', 'č', 'ĉ', 'ċ'),
            'd'    => array('ď', 'ð', 'đ', 'ƌ', 'ȡ', 'ɖ', 'ɗ', 'ᵭ', 'ᶁ', 'ᶑ', 'д', 'δ', 'د', 'ض', 'ဍ', 'ဒ', 'დ'),
            'e'    => array('é', 'è', 'ẻ', 'ẽ', 'ẹ', 'ê', 'ế', 'ề', 'ể', 'ễ', 'ệ', 'ë', 'ē', 'ę', 'ě', 'ĕ', 'ė', 'ε', 'έ', 'ἐ', 'ἑ', 'ἒ', 'ἓ', 'ἔ', 'ἕ', 'ὲ', 'έ', 'е', 'ё', 'э', 'є', 'ə', 'ဧ', 'ေ', 'ဲ', 'ე', 'ए', 'إ', 'ئ'),
            'f'    => array('ф', 'φ', 'ف', 'ƒ', 'ფ'),
            'g'    => array('ĝ', 'ğ', 'ġ', 'ģ', 'г', 'ґ', 'γ', 'ဂ', 'გ', 'گ'),
            'h'    => array('ĥ', 'ħ', 'η', 'ή', 'ح', 'ه', 'ဟ', 'ှ', 'ჰ'),
            'i'    => array('í', 'ì', 'ỉ', 'ĩ', 'ị', 'î', 'ï', 'ī', 'ĭ', 'į', 'ı', 'ι', 'ί', 'ϊ', 'ΐ', 'ἰ', 'ἱ', 'ἲ', 'ἳ', 'ἴ', 'ἵ', 'ἶ', 'ἷ', 'ὶ', 'ί', 'ῐ', 'ῑ', 'ῒ', 'ΐ', 'ῖ', 'ῗ', 'і', 'ї', 'и', 'ဣ', 'ိ', 'ီ', 'ည်', 'ǐ', 'ი', 'इ'),
            'j'    => array('ĵ', 'ј', 'Ј', 'ჯ', 'ج'),
            'k'    => array('ķ', 'ĸ', 'к', 'κ', 'Ķ', 'ق', 'ك', 'က', 'კ', 'ქ', 'ک'),
            'l'    => array('ł', 'ľ', 'ĺ', 'ļ', 'ŀ', 'л', 'λ', 'ل', 'လ', 'ლ'),
            'm'    => array('м', 'μ', 'م', 'မ', 'მ'),
            'n'    => array('ñ', 'ń', 'ň', 'ņ', 'ŉ', 'ŋ', 'ν', 'н', 'ن', 'န', 'ნ'),
            'o'    => array('ó', 'ò', 'ỏ', 'õ', 'ọ', 'ô', 'ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'ơ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ', 'ø', 'ō', 'ő', 'ŏ', 'ο', 'ὀ', 'ὁ', 'ὂ', 'ὃ', 'ὄ', 'ὅ', 'ὸ', 'ό', 'о', 'و', 'θ', 'ို', 'ǒ', 'ǿ', 'º', 'ო', 'ओ'),
            'p'    => array('п', 'π', 'ပ', 'პ', 'پ'),
            'q'    => array('ყ'),
            'r'    => array('ŕ', 'ř', 'ŗ', 'р', 'ρ', 'ر', 'რ'),
            's'    => array('ś', 'š', 'ş', 'с', 'σ', 'ș', 'ς', 'س', 'ص', 'စ', 'ſ', 'ს'),
            't'    => array('ť', 'ţ', 'т', 'τ', 'ț', 'ت', 'ط', 'ဋ', 'တ', 'ŧ', 'თ', 'ტ'),
            'u'    => array('ú', 'ù', 'ủ', 'ũ', 'ụ', 'ư', 'ứ', 'ừ', 'ử', 'ữ', 'ự', 'û', 'ū', 'ů', 'ű', 'ŭ', 'ų', 'µ', 'у', 'ဉ', 'ု', 'ူ', 'ǔ', 'ǖ', 'ǘ', 'ǚ', 'ǜ', 'უ', 'उ'),
            'v'    => array('в', 'ვ', 'ϐ'),
            'w'    => array('ŵ', 'ω', 'ώ', 'ဝ', 'ွ'),
            'x'    => array('χ', 'ξ'),
            'y'    => array('ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ', 'ÿ', 'ŷ', 'й', 'ы', 'υ', 'ϋ', 'ύ', 'ΰ', 'ي', 'ယ'),
            'z'    => array('ź', 'ž', 'ż', 'з', 'ζ', 'ز', 'ဇ', 'ზ'),
            'aa'   => array('ع', 'आ', 'آ'),
            'ae'   => array('ä', 'æ', 'ǽ'),
            'ai'   => array('ऐ'),
            'at'   => array('@'),
            'ch'   => array('ч', 'ჩ', 'ჭ', 'چ'),
            'dj'   => array('ђ', 'đ'),
            'dz'   => array('џ', 'ძ'),
            'ei'   => array('ऍ'),
            'gh'   => array('غ', 'ღ'),
            'ii'   => array('ई'),
            'ij'   => array('ĳ'),
            'kh'   => array('х', 'خ', 'ხ'),
            'lj'   => array('љ'),
            'nj'   => array('њ'),
            'oe'   => array('ö', 'œ', 'ؤ'),
            'oi'   => array('ऑ'),
            'oii'  => array('ऒ'),
            'ps'   => array('ψ'),
            'sh'   => array('ш', 'შ', 'ش'),
            'shch' => array('щ'),
            'ss'   => array('ß'),
            'sx'   => array('ŝ'),
            'th'   => array('þ', 'ϑ', 'ث', 'ذ', 'ظ'),
            'ts'   => array('ц', 'ც', 'წ'),
            'ue'   => array('ü'),
            'uu'   => array('ऊ'),
            'ya'   => array('я'),
            'yu'   => array('ю'),
            'zh'   => array('ж', 'ჟ', 'ژ'),
            '(c)'  => array('©'),
            'A'    => array('Á', 'À', 'Ả', 'Ã', 'Ạ', 'Ă', 'Ắ', 'Ằ', 'Ẳ', 'Ẵ', 'Ặ', 'Â', 'Ấ', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ', 'Å', 'Ā', 'Ą', 'Α', 'Ά', 'Ἀ', 'Ἁ', 'Ἂ', 'Ἃ', 'Ἄ', 'Ἅ', 'Ἆ', 'Ἇ', 'ᾈ', 'ᾉ', 'ᾊ', 'ᾋ', 'ᾌ', 'ᾍ', 'ᾎ', 'ᾏ', 'Ᾰ', 'Ᾱ', 'Ὰ', 'Ά', 'ᾼ', 'А', 'Ǻ', 'Ǎ'),
            'B'    => array('Б', 'Β', 'ब'),
            'C'    => array('Ç', 'Ć', 'Č', 'Ĉ', 'Ċ'),
            'D'    => array('Ď', 'Ð', 'Đ', 'Ɖ', 'Ɗ', 'Ƌ', 'ᴅ', 'ᴆ', 'Д', 'Δ'),
            'E'    => array('É', 'È', 'Ẻ', 'Ẽ', 'Ẹ', 'Ê', 'Ế', 'Ề', 'Ể', 'Ễ', 'Ệ', 'Ë', 'Ē', 'Ę', 'Ě', 'Ĕ', 'Ė', 'Ε', 'Έ', 'Ἐ', 'Ἑ', 'Ἒ', 'Ἓ', 'Ἔ', 'Ἕ', 'Έ', 'Ὲ', 'Е', 'Ё', 'Э', 'Є', 'Ə'),
            'F'    => array('Ф', 'Φ'),
            'G'    => array('Ğ', 'Ġ', 'Ģ', 'Г', 'Ґ', 'Γ'),
            'H'    => array('Η', 'Ή', 'Ħ'),
            'I'    => array('Í', 'Ì', 'Ỉ', 'Ĩ', 'Ị', 'Î', 'Ï', 'Ī', 'Ĭ', 'Į', 'İ', 'Ι', 'Ί', 'Ϊ', 'Ἰ', 'Ἱ', 'Ἳ', 'Ἴ', 'Ἵ', 'Ἶ', 'Ἷ', 'Ῐ', 'Ῑ', 'Ὶ', 'Ί', 'И', 'І', 'Ї', 'Ǐ', 'ϒ'),
            'K'    => array('К', 'Κ'),
            'L'    => array('Ĺ', 'Ł', 'Л', 'Λ', 'Ļ', 'Ľ', 'Ŀ', 'ल'),
            'M'    => array('М', 'Μ'),
            'N'    => array('Ń', 'Ñ', 'Ň', 'Ņ', 'Ŋ', 'Н', 'Ν'),
            'O'    => array('Ó', 'Ò', 'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ố', 'Ồ', 'Ổ', 'Ỗ', 'Ộ', 'Ơ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ', 'Ø', 'Ō', 'Ő', 'Ŏ', 'Ο', 'Ό', 'Ὀ', 'Ὁ', 'Ὂ', 'Ὃ', 'Ὄ', 'Ὅ', 'Ὸ', 'Ό', 'О', 'Θ', 'Ө', 'Ǒ', 'Ǿ'),
            'P'    => array('П', 'Π'),
            'R'    => array('Ř', 'Ŕ', 'Р', 'Ρ', 'Ŗ'),
            'S'    => array('Ş', 'Ŝ', 'Ș', 'Š', 'Ś', 'С', 'Σ'),
            'T'    => array('Ť', 'Ţ', 'Ŧ', 'Ț', 'Т', 'Τ'),
            'U'    => array('Ú', 'Ù', 'Ủ', 'Ũ', 'Ụ', 'Ư', 'Ứ', 'Ừ', 'Ử', 'Ữ', 'Ự', 'Û', 'Ū', 'Ů', 'Ű', 'Ŭ', 'Ų', 'У', 'Ǔ', 'Ǖ', 'Ǘ', 'Ǚ', 'Ǜ'),
            'V'    => array('В'),
            'W'    => array('Ω', 'Ώ', 'Ŵ'),
            'X'    => array('Χ', 'Ξ'),
            'Y'    => array('Ý', 'Ỳ', 'Ỷ', 'Ỹ', 'Ỵ', 'Ÿ', 'Ῠ', 'Ῡ', 'Ὺ', 'Ύ', 'Ы', 'Й', 'Υ', 'Ϋ', 'Ŷ'),
            'Z'    => array('Ź', 'Ž', 'Ż', 'З', 'Ζ'),
            'AE'   => array('Ä', 'Æ', 'Ǽ'),
            'CH'   => array('Ч'),
            'DJ'   => array('Ђ'),
            'DZ'   => array('Џ'),
            'GX'   => array('Ĝ'),
            'HX'   => array('Ĥ'),
            'IJ'   => array('Ĳ'),
            'JX'   => array('Ĵ'),
            'KH'   => array('Х'),
            'LJ'   => array('Љ'),
            'NJ'   => array('Њ'),
            'OE'   => array('Ö', 'Œ'),
            'PS'   => array('Ψ'),
            'SH'   => array('Ш'),
            'SHCH' => array('Щ'),
            'SS'   => array('ẞ'),
            'TH'   => array('Þ'),
            'TS'   => array('Ц'),
            'UE'   => array('Ü'),
            'YA'   => array('Я'),
            'YU'   => array('Ю'),
            'ZH'   => array('Ж'),
            ' '    => array("\xC2\xA0", "\xE2\x80\x80", "\xE2\x80\x81", "\xE2\x80\x82", "\xE2\x80\x83", "\xE2\x80\x84", "\xE2\x80\x85", "\xE2\x80\x86", "\xE2\x80\x87", "\xE2\x80\x88", "\xE2\x80\x89", "\xE2\x80\x8A", "\xE2\x80\xAF", "\xE2\x81\x9F", "\xE3\x80\x80"),
        );
        foreach($unicode as $key => $val){
            $string = str_replace($val, $key, $string);
        }
        $special = array('+','”','“',',', '.',':',';',"'",'"','/','!','@','$','%','^','&','*','(',')','=','\\','[',']','?','>','<','{','}');
        $string = str_replace($special, '', $string);

        $string = strtolower($string);
        $string = str_replace("ß", "ss", $string);
        $string = preg_replace("/[^_a-zA-Z0-9 -]/", "", $string);
        $string = str_replace(array('%20', ' '), $space, $string);
        $string = str_replace($space . $space . $space . $space, $space, $string);
        $string = str_replace($space . $space . $space, $space, $string);
        $string = str_replace($space . $space, $space, $string);
        return $string;
    }
    function sanitizeTitleBackup($string, $space = '-'){
        if (!$string) return false;
        $utf8 = array(
            'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'd' => 'đ|Đ',
            'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'i' => 'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',
            'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
            'y' => 'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
        );
        foreach ($utf8 as $ascii => $uni) {
            if (preg_match('/[' . $uni . ']/', $string)) {
                $string = preg_replace('/($uni)/i', $ascii, $string);
            }
        }
        $string = strtolower($string);
        $string = str_replace("ß", "ss", $string);
        $string = str_replace("%", "", $string);
        $string = preg_replace("/[^_a-zA-Z0-9 -]/", "", $string);
        $string = str_replace(array('%20', ' '), $space, $string);
        $string = str_replace($space . $space . $space . $space, $space, $string);
        $string = str_replace($space . $space . $space, $space, $string);
        $string = str_replace($space . $space, $space, $string);
        return $string;
    }
}
if(!function_exists('logE')){
    function logE($msg){
        error_log(date('[Y:m:d H:i:s] ') . print_r($msg, true) . "\n", 3, FCPATH . 'system-'.date('Y-m-d').'.log');
    }
}
if(!function_exists('vcc_paging')){
    function vcc_paging($total_page, $path='', $param_page='page', $config=array()){
        $config['first_text'] = isset($config['first_text']) ? $config['first_text'] : 'Đầu';
        $config['prev_text'] = isset($config['prev_text']) ? $config['prev_text'] : 'Trước';
        $config['next_text'] = isset($config['next_text']) ? $config['next_text'] : 'Sau';
        $config['last_text'] = isset($config['last_text']) ? $config['last_text'] : 'Cuối';

        $config['first_show'] = isset($config['first_show']) ? $config['first_show'] : true;
        $config['last_show'] = isset($config['last_show']) ? $config['last_show'] : true;
        ?>
        <div class="col-sm-12">
            <div class="dataTables_paginate paging_simple_numbers">
                <ul class="pagination">
                    <?php
                    if($total_page>1) {
                        $page = isset($_GET[$param_page]) ? $_GET[$param_page] : 1;
                        $search = isset($_GET['s'])&&$_GET['s'] ? '?s='.urlencode($_GET['s']) : '';
                        $pre_page = $search ? '&' : '?';
                        $status_uri = '';
                        if(isset($_GET['status'])&&is_numeric($_GET['status'])){
                            $status_uri = $pre_page.'status='.$_GET['status'];
                            $pre_page = '&';
                        }

                        if($page<=3){
                            $start = 1;
                            $end = $total_page>=5 ? 5 : $total_page;
                        }else if($page>=($total_page-2) && $page<=$total_page){
                            $start = ($total_page-5)<=1 ? 1 : $total_page-5;
                            $end = $total_page;
                        }else{
                            $start = ($page-2)<=1 ? 1 : ($page-2);
                            $end = ($page+2)>=$total_page ? $total_page : ($page+2);
                        }?>

                        <?php if($page>1){?>
                            <?php if($config['first_show']){?>
                            <li class="paginate_button previous">
                                <a href="<?php
                                echo base_url($path.$search.$status_uri) ?>"><?php echo $config['first_text'];?></a>
                            </li>
                            <?php }?>
                            <li class="paginate_button previous">
                                <a href="<?php
                                $page_prev = $page>1 ? $pre_page.$param_page.'='.($page-1) : '';
                                echo base_url($path.$search.$status_uri.$page_prev) ?>"><?php echo $config['prev_text'];?></a>
                            </li>
                        <?php }?>

                        <?php for ($i = $start; $i <= $end; $i++) {
                            $page_url = $i>1 ? $pre_page.$param_page.'='.$i : '';
                            ?>
                            <li class="paginate_button<?php if ($page == $i){echo ' active';} ?>"><a href="<?php
                                echo base_url($path.$search.$status_uri.$page_url) ?>"><?php echo $i;?></a></li>
                        <?php }?>

                        <?php if($page<$total_page){?>
                            <li class="paginate_button next">
                                <a href="<?php
                                $page_next = $page<$total_page ? $pre_page.$param_page.'='.($page+1) : '';
                                echo base_url($path.$search.$status_uri.$page_next)
                                ?>"><?php echo $config['next_text'];?></a>
                            </li>
                            <?php if($config['last_show']){?>
                            <li class="paginate_button previous">
                                <a href="<?php
                                echo base_url($path.$search.$status_uri.$pre_page.$param_page.'='.$total_page) ?>"><?php echo $config['last_text'];?></a>
                            </li>
                            <?php }?>
                        <?php }?>
                    <?php }?>
                </ul>
            </div>
        </div>
    <?php }
}
if(!function_exists('post_data')){
    function post_data($name, $data, $def=null){
        if(isset($_POST[$name])){
            $CI =& get_instance();
            $val = $CI->input->post($name);
            return is_string($val) ? htmlentities($val) : $val;
        }else{
            if($data && isset($data->$name)){
                return is_string($data->$name) ? htmlentities($data->$name) : $data->$name;
            }else{
                return $def;
            }
        }
    }
}
if(!function_exists('get_data')){
    function get_data($name, $data, $def=null){
        if(isset($_GET[$name])){
            $CI =& get_instance();
            $val = $CI->input->get($name);
            return is_string($val) ? htmlentities($val) : $val;
        }else{
            if(isset($data)&&!empty($data)){
                return is_string($data->$name) ? htmlentities($data->$name) : $data->$name;
            }else{
                return $def;
            }
        }
    }
}
if(!function_exists('is_mobile')) {
    function is_mobile(){
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
}
if(!function_exists('get_meta_robot')){
    function get_meta_robot(){
        return array(
            'index, follow',
            'index, nofollow',
            'noindex, follow',
            'noindex, follow',
            'noindex, nofollow'
        );
    }
}

if(!function_exists('get_tip_link')){
    function get_tip_link($data){
        return base_url('bi-kip-di-bo-'.$data->id);
        //return 'javascript:void(0)';
    }
}
if(!function_exists('get_thumbnail_url')){
    function get_thumbnail_url($path, $w=0, $h=0, $more_param=''){
        if(!$path) return '';
        if(strpos($path, 'http')!==false){
            return $path.$more_param;
        }
        if(substr($path, 0, 1)==='/'){
            $path = substr($path, 1);
        }
        $full_url = base_url($path);
        //$full_path = str_replace( base_url('/'), FCPATH, $full_url);
        //if(file_exists($full_path)===false) return '';
        if($w && $h){
            $info = pathinfo($path);
            $new_name = $info['filename'].'-'.$w.'x'.$h.'.'.$info['extension'];
            $new_path = str_replace($info['basename'], $new_name, $path);
            if(file_exists(FCPATH.$new_path)===false){
                return $full_url.$more_param;
            }
            return base_url($new_path.$more_param);
        }else{
            return $full_url.$more_param;
        }
    }
}
if(!function_exists('get_fb_thumbnail')){
    function get_fb_thumbnail($data){
        if(!$data) return '';
        $path = 'uploads/'.'fb-bi-kip-di-bo-'.$data->id.'.png';
        if(file_exists(FCPATH.$path)){
            return base_url($path);
        }else{
            return base_url('public/images/popup/default.png');
        }
    }
}
if(!function_exists('get_weeks')){
    function get_weeks(){
        return array(
            1 => array('start'=> '2017-06-29 00:00:00', 'end' => '2017-07-06 23:59:59', 'title'=>'Đi bộ cùng thú cưng', 'slug'=>'di-bo-cung-thu-cung'),
            2 => array('start'=> '2017-07-07 00:00:00', 'end' => '2017-07-13 23:59:59', 'title'=>'Đi bộ cùng người yêu','slug'=>'di-bo-cung-nguoi-yeu'),
            3 => array('start'=> '2017-07-14 00:00:00', 'end' => '2017-07-20 23:59:59', 'title'=>'Đi bộ ngược','slug'=>'di-bo-nguoc'),
            4 => array('start'=> '2017-07-21 00:00:00', 'end' => '2017-07-27 23:59:59', 'title'=>'Đi bộ cân bằng','slug'=>'di-bo-can-bang')
        );
    }
}
if(!function_exists('get_week_current')){
    function get_week_current($date=''){
        $weeks = get_weeks();
        $curr = $date ? strtotime($date) : time();
        $result = 1;
        foreach($weeks as $w=>$item){
            if($curr>=strtotime($item['start']) && $curr<=strtotime($item['end'])){
                $result = $w;
            }
        }
        return $result;
    }
}
if(!function_exists('get_prize_types')){
    function get_prize_types(){
        return array(1=>'loa bluetooth', 2=>'đồng hồ xiaomi', 3=>'Balo Revive');
    }
}