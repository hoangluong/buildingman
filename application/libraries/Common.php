<?php

class Common
{
    public static function is_ajax()
    {

        if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
            return false;
        }
        return true;
    }

    public static function payment_method($id = 0)
    {
        $arr = array(
            1 => 'Cash',
            2 => 'Cashier\'s check',
            3 => 'Check',
            4 => 'Credit card',
            5 => 'Direct deposit',
            6 => 'Electronic payment',
            7 => 'Money order'
        );
        if($id > 0)
            return $arr[$id];
        return $arr;
    }
    public static function payment_type($id = 0)
    {
        $arr = array(
            1 => 'Refund',
            2 => 'Payment',
            3 => 'Charge',
            4 => 'Credit',
        );
        if($id > 0)
            return $arr[$id];
        return $arr;
    }
    public static function bank_account($id = 0)
    {
        $arr = array(
            1 => 'Trust account',
            2 => 'Company savings',
            3 => 'Company checking',
            4 => 'Lincoln operating',
            5 => 'Lincoln reserve',
            6 => 'Rent Account',
        );
        if($id > 0)
            return $arr[$id];
        return $arr;
    }

    public static function building_type($id = 0){
        $arr = array(
            1 => 'Condo/Townhome',
            2 => 'Multi-Family',
            3 => 'Single-Family'
        );
        if($id > 0)
            return $arr[$id];
        return $arr;
    }

    public static function frequency($id = 0){
        $arr = array(
            1 => 'Daily',
            2 => 'Weekly',
            3 => 'Every two weeks',
            4 => 'Monthly',
            5 => 'Every two months',
            6 => 'Quarterly',
            7 => 'Every six months',
            8 => 'Yearly',
            9 => 'One time'
        );
        if($id > 0)
            return $arr[$id];
        return $arr;
    }
    public static function rent_roll_type($id = 0){
        $arr = array(
            1 => 'Fixed',
            2 => 'Fixed w/rollover',
            3 => 'At win(month to month)',

        );
        if($id > 0)
            return $arr[$id];
        return $arr;
    }

}