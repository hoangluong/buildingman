<?php

class Rentroll_model extends Base_model
{

    protected $table = 'rentrolls';

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_cond($cond)
    {
        if (isset($cond['s']) && $cond['s']) {
            $this->db->group_start();
            $this->db->or_like('name', $cond['s']);
            $this->db->group_end();
            unset($cond['s']);
        }
        return $cond;
    }

    function find($cond, $page, $per_page, $order = array())
    {
        $cond = $this->get_cond($cond);
        return parent::find($cond, $page, $per_page, $order);
    }

    function getAllRentroll(){
        $this->db->select('R.*, B.address_street as building_address_street, U.number as unit_number, B.type as building_type');
        $this->db->from('rentrolls R');
        $this->db->join('Buildings B', 'B.id = R.building_id', 'left');
        $this->db->join('units U', 'U.id = R.unit_id', 'left');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return (isset($return[0])) ? $return : null;
    }
}

?>