<?php $this->load->view('user/header');?>
    <div class="container" style="margin: 0;position: absolute;top: 50%;left: 50%;margin-right: -50%;transform: translate(-50%, -50%);padding: 10px">
        <img src="<?php echo site_url('/public/images/logo.png')?>" style="display: block; margin-right: auto; margin-left: auto;margin-bottom: 25px">
        <div class="container">
            <div class="row">
                <form class="form-horizontal form-auth" name="SignInform" action="" method="post">
                    <input type="hidden" name="token" id="token" value="<?php echo $token ?>"/>
                    <h1 class="page-title">Reset password</h1>
                    <?php if (isset($message) && $message) { ?>
                        <!-- alert alert-info alert-dismissible -->
                        <div style="display: block;"
                             class="alert <?php echo $message['success'] ? 'alert-info ' : 'alert-danger '; ?>alert-dismissible"
                             id="message">
                            <button type="button" id="btn-close-msg" class="close" data-dismiss="alert"
                                    aria-hidden="true">×
                            </button>
                            <p id="message-content"><?php echo $message['msg']; ?></p>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="password" class="control-label col-xs-4">New password</label>
                        <div class="col-xs-8">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter your password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="re_password" class="control-label col-xs-4">Confirm</label>
                        <div class="col-xs-8">
                            <input type="password" class="form-control" name="re_password" id="re_password" placeholder="Confirm your password">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-offset-4 col-xs-8">
                            <button type="submit" class="btn btn-default">Send</button>
                            <a href="<?php echo base_url('login')?>" class="btn btn-link"><?php echo htmlentities('<<');?> Back</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $this->load->view('user/footer');?>