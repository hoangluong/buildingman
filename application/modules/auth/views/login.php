<?php $this->load->view('auth/header');?>
<div class="container" style="margin: 0;position: absolute;top: 50%;left: 50%;margin-right: -50%;transform: translate(-50%, -50%);padding: 10px">
    <img src="<?php echo site_url('/public/images/logo-admicro.png')?>" style="display: block; margin-right: auto; margin-left: auto;margin-bottom: 25px">
    <div class="container">
        <form class="form-horizontal form-auth" name="SignInform" action="" method="post">
            <input type="hidden" name="frmToken" id="frmToken" value="<?php echo $frmToken ?>"/>
            <h1 class="page-title">Login</h1>
            <?php if (isset($message) && $message) { ?>
                <!-- alert alert-info alert-dismissible -->
                <div style="display: block;"
                     class="alert <?php echo $message['success'] ? 'alert-info ' : 'alert-danger '; ?>alert-dismissible"
                     id="message">
                    <button type="button" id="btn-close-msg" class="close" data-dismiss="alert"
                            aria-hidden="true">×
                    </button>
                    <p id="message-content"><?php echo $message['msg']; ?></p>
                </div>
            <?php } ?>
            <div class="form-group">
                <label for="userName" class="control-label col-xs-4" >Username</label>
                <div class="col-xs-8">
                    <input type="text" class="form-control" name='username' id='userName' placeholder="Username">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="control-label col-xs-4">Password</label>
                <div class="col-xs-8">
                    <input type="password" class="form-control" name='password' id='password'
                           placeholder="Password">
                </div>
            </div>
            <!--<div class="form-group">
                <div class="checkbox col-xs-9" style="float: right;">
                    <label><input name="remember_me" type="checkbox" value="1">Remember me</label>
                </div>
            </div>-->

            <div class="form-group">
                <div class="col-xs-offset-4 col-xs-8">
                    <button type="submit" class="btn btn-default">Login</button>
                    <a href="<?php echo base_url('user/forget-password')?>" class="btn btn-link">Forget password</a>
                </div>
            </div>
        </form>
    </div>
</div>
<?php $this->load->view('auth/footer');?>