<?php if (!isset($user) || !$user) die;
$role = $user->role;
$name = post_data('name', $user, '');
$email = post_data('email', $user, '');
$phone = post_data('phone', $user, '');
$username = post_data('username', $user, '');
$role = post_data('role', $user, 0);
$status = post_data('status', $user, 0);
$type = isset($_POST['type']) ? $_POST['type'] : '';
?>
<div class="row">
    <div class="col-xs-6">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Account information</h3>
            </div>
            <div class="box-body">
                <form action="" method="post">
                    <?php if (isset($message) && $type==='account') { ?>
                        <!-- alert alert-info alert-dismissible -->
                        <div style="display: block;"
                             class="alert <?php echo $message['success'] ? 'alert-info ' : 'alert-danger '; ?>alert-dismissible"
                             id="message">
                            <button type="button" id="btn-close-msg" class="close" data-dismiss="alert"
                                    aria-hidden="true">×
                            </button>
                            <p id="message-content"><?php echo $message['msg']; ?></p>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="username">Username:</label>
                        <input name="username" class="form-control"<?php echo $username ? ' readonly' : '';?> value="<?php echo $username; ?>">
                    </div>
                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" name="password" id="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="re_password">Re-type Password:</label>
                        <input type="password" name="re_password" id="re_password" class="form-control">
                    </div>
                    <input type="hidden" name="type" value="account">
                    <button type="submit" class="btn btn-info">Save</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Personal Infomation</h3>
            </div>
            <div class="box-body">
                <form action="" method="post">
                    <?php if (isset($message) && $type==='info') { ?>
                        <!-- alert alert-info alert-dismissible -->
                        <div style="display: block;"
                             class="alert <?php echo $message['success'] ? 'alert-info ' : 'alert-danger '; ?>alert-dismissible"
                             id="message">
                            <button type="button" id="btn-close-msg" class="close" data-dismiss="alert"
                                    aria-hidden="true">×
                            </button>
                            <p id="message-content"><?php echo $message['msg']; ?></p>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="name">Full name</label>
                        <input type="text" name="name" id="name" value="<?php echo $name; ?>"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="text" name="email" id="email" value="<?php echo $email; ?>"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone:</label>
                        <input type="text" name="phone" id="phone" value="<?php echo $phone; ?>"
                               class="form-control">
                    </div>
                    <input type="hidden" name="type" value="info">
                    <button type="submit" class="btn btn-info">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>