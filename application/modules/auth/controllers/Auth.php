<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends Front_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('auth/user_model');
        $this->load->model('auth/reset_model');
    }
    private function redirect_back(){
        $current_url = $this->session->userdata('current_url');
        $this->session->unset_userdata('current_url');
        if($current_url){
            redirect($current_url);
        }else{
            redirect('/tips');
        }
    }

    public function index()
    {

        if($this->session->userdata('user_id')) $this->redirect_back();
        
        $data = array();
        if($_POST){
            $result = $this->checkLogin();
            if($result!==true){
                $data['message'] = $result;
            }
        }

        $data['frmToken'] = uniqid();
        $this->session->set_userdata('frmToken', $data['frmToken']);
        $this->load->view('auth/login',$data);
    }
    private function checkLogin(){
        $username = $this->input->post('username', true);
        $password = $this->input->post('password', true);
        $frmToken = $this->input->post('frmToken');
        if(!$username || !$password){
            return $this->message('Please input your username and password');
        }
        if(!$frmToken || $this->input->post('frmToken') !== $this->session->userdata('frmToken')) {
            return $this->message('Timeout. please reload the page !');
        }

        $user_data = $this->user_model->checkLogin($username, $password);
        if(!$user_data){
            return $this->message('Username or password is incorrect!');
        }
        if(!$user_data->status){
            return $this->message('Your account is inactive.');
        }
        if($user_data->role!=1){
            return $this->message('You do not have permission to access!');
        }
        $this->session->set_userdata('user_id', $user_data->id);
        $this->session->set_userdata('user_data', (array)$user_data);
        $this->redirect_back();
        return true;
    }
    function reset_password(){
        $data = array();
        if($this->session->userdata('user_id')){
            $this->redirect_back();
        }

        $token = isset($_REQUEST['token']) ? $_REQUEST['token'] : '';
        $data['message'] = $this->validResetPassword();

        $data['token'] = $token;
        $this->load->view('auth/reset_password',$data);
    }
    private function validResetPassword(){
        $token = isset($_REQUEST['token']) ? $_REQUEST['token'] : '';
        if(!$token) return $this->message('Token is not valid');

        $reset_data = $this->reset_model->find_by('token', $token);
        if(!$reset_data) return $this->message('Token is not valid');

        $tokenTimeOut = strtotime($reset_data->created_on) + 86400;//1 day
        if($tokenTimeOut<time()) return $this->message('Token is expired');

        $user = $this->user_model->find_by('email', $reset_data->email);
        if(!$user) return $this->message('Account is not exists');

        if($_POST){
            $message = $this->save_setting_account($user->id);
            if($message['success']){
                $this->reset_model->delete_by('email', $reset_data->email);
                return $this->message('Changed password. <a href="'.base_url('login').'">Return login</a>', true);
            }else{
                return $message;
            }
        }
        return 0;
    }
    function forget_password(){
        if($this->session->userdata('user_id')){
            $this->redirect_back();
        }
        $data = array();
        if($_POST){
            $data['message'] = $this->forgetPasswordAction();
        }
        $data['frmToken'] = uniqid();
        $this->load->view('auth/forget_password',$data);
    }
    private function forgetPasswordAction(){
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $check_email = $this->user_model->find_by('email', $email);
        if(!$check_email){
            return $this->message('Email is not exists');
        }
        $token = $this->generateToken($email);
        if(is_array($token)){
            return $token;
        }
        $this->sendMailReset($email, $token);
        return $this->message('Send mail success. Please check inbox or spam mail.', true);
    }
    private function sendMailReset($email, $token){
        $this->load->model('setting_model');
        $data_config = $this->setting_model->find_by('config_key', 'email');
        if(!$data_config) return 0;
        $config = json_decode(html_entity_decode($data_config->config_value), true);
        if(!$config['recipients']
            || !$config['smtp_user']
            || !$config['smtp_pass']) return 0;

        unset($config['recipients']);
        $sender = $config['sender'];
        unset($config['sender']);

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['smtp_user'], $sender);
        $list = array($email);
        $this->email->to($list);
        $this->email->subject('['.DEFAULT_TITLE.'] Quên mật khẩu');
        $link = base_url('reset-password?token='.$token);
        $msg = '<p>Hi , You have just requested to reset your password.<br/>';
        $msg .= '<p>Please click into link below to continue</p><br/>';
        $msg .= '<a href="'.$link.'">'.$link.'</a><br/>';
        $msg .= '<p>If not your account, please ignore this email.</p><br/>';
        $msg .= '<p>Cheers!</p>';
        $this->email->message($msg);
        $this->email->send();

    }
    private function generateToken($email){
        $static_str='FORGET';
        $currentTimeSeconds = date("mdY_His");
        $token_id=$static_str.$email.$currentTimeSeconds;
        $data = array(
            'email' => $email,
            'token' => md5($token_id)
        );
        $check_token = $this->reset_model->find_by('email', $email);
        if($check_token){
            return $check_token->token;
        }
        $this->reset_model->add($data);
        $check_token = $this->reset_model->find_by('email', $email);
        if($check_token){
            return $check_token->token;
        }else{
            return $this->message('Have error when insert token');
        }
    }
    function setting(){
        if(!$this->session->userdata('admin_id')) redirect('login');
        $uid = $this->session->userdata('admin_id');
        $temp['page_title'] = 'Account Settings';
        $temp['user'] = $this->user_model->get($uid);
        if($_POST){
            $message = $this->save_setting();
            $temp['message'] = $message;
        }
        $temp['template'] = 'auth/setting';
        $this->load->view('layout.php',$temp);
    }
    private function save_setting(){
        $uid = $this->session->userdata('admin_id');
        if(!$uid) redirect('login');
        $type = isset($_POST['type']) ? stripslashes($_POST['type']) : '';
        if($type==='info'){
            return $this->save_setting_info($uid);
        }elseif($type==='account'){
            return $this->save_setting_account($uid);
        }else{
            return $this->message('Input is not valid');
        }
    }
    private function save_setting_info($uid){
        $email = isset($_POST['email']) ? stripslashes($_POST['email']) : '';
        $phone = isset($_POST['phone']) ? stripslashes($_POST['phone']) : '';
        if ($email && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->message('Email is not valid');
        }
        if($phone && !preg_match( '/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i', $phone )){
            return $this->message('Phone is not valid');
        }
        $request = $_POST;
        unset($request['type']);
        $data = array();
        if($request){
            foreach($request as $field=>$val){
                $data[$field] = stripslashes($val);
            }
            $res = $this->user_model->update($uid, $data);
            if(!$res) return $this->message('Have error. Please try again.');
            return $this->message('Save success.', true);
        }else{
            return $this->message('Input is null.');
        }
    }
    private function save_setting_account($uid){
        $password = isset($_POST['password']) ? stripslashes($_POST['password']) : '';
        $re_password = isset($_POST['re_password']) ? stripslashes($_POST['re_password']) : '';
        $data = array();
        $fb_id = $this->input->post('fb_id', true);
        if($fb_id){
            $data['fb_id'] = $fb_id;
        }
        $username = $this->input->post('username', true);
        if($fb_id){
            $data['username'] = $username;
        }
        if($password){
            if($re_password!=$password){
                return $this->message('Retype password is incorrect');
            }
            $data['password'] = hash('sha512', $password);
        }
        if($data){
            $res = $this->user_model->update($uid, $data);
            if(!$res) return $this->message('Have error. Please try again.');
        }
        return $this->message('Save success.', true);
    }
    public function logout()
    {
        $this->session->unset_userdata('admin_id');
        $this->session->unset_userdata('user_data');
        redirect(site_url('login'));
    }
}
