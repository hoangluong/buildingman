<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends Base_model {

    protected $table = 'user_serving';
    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }
    function get_cond($cond){
        if(isset($cond['s']) && $cond['s']){
            $this->db->group_start();
            $this->db->or_like('username', $cond['s']);
            $this->db->or_like('name', $cond['s']);
            $this->db->or_like('email', $cond['s']);
            $this->db->group_end();
            unset($cond['s']);
        }
        return $cond;
    }
    function find($cond, $page, $per_page, $order=array()){
        $cond = $this->get_cond($cond);
        return parent::find($cond, $page, $per_page, $order);
    }
    function total($cond){
        $cond = $this->get_cond($cond);
        return parent::total($cond);
    }
    public function checkLogin($username, $password){
        $this->db->where('username' , $username );
         $this->db->where('password', md5($password) );
        $res = $this->db->get($this->table, 1)->result();
	 
        if(!$res) return 0;
        return $res[0];
    }
}