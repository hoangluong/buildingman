<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Admin_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('auth/user_model');
        $user_data = $this->session->userdata('user_data');
        if($user_data->role!=1) die('Permission is invalid');
    }

    public function index(){
        $s = stripslashes($this->input->get('s', true));
        $page = stripslashes($this->input->get('page', true));
        $page = $page ? $page : 1;
        $cond = array();
        if($s){
            $cond['s'] = $this->input->get('s', true);
        }
        $contacts = $this->user_model->find($cond, $page, $this->per_page, array('id'=>'desc'));
        $total = $this->user_model->total($cond);
        $temp['total_page'] = $total%$this->per_page==0 ? $total/$this->per_page : intval($total/$this->per_page)+1;

        $temp['data'] = $contacts;

        $temp['page_title'] = 'Account listing';
        $temp['template'] = 'user/list';
        $this->load->view('layout.php',$temp);
    }
    function add(){
        $temp['page_title'] = 'Add Account';

        if($_POST){
            $message = $this->save_user();
            $temp['message'] = $message;
        }
        $temp['template'] = 'user/add';
        $this->load->view('layout.php',$temp);
    }
    function edit($uid){
        if(!$uid || !is_numeric($uid) || $uid<0) redirect('manage');
        $temp['page_title'] = 'Sửa thông tin thành viên';
        if($_POST){
            $message = $this->save_user();
            $temp['message'] = $message;
        }
        $temp['data'] = $this->user_model->get($uid);
        if(!$temp['data']) redirect('user');
        $temp['template'] = 'user/edit';
        $this->load->view('layout.php',$temp);
    }
    private function save_user(){
        $uid = $this->input->post('id', true);
        $email = $this->input->post('email', true);
        $phone = $this->input->post('phone', true);
        $username = $this->input->post('username', true);
        $password = $this->input->post('password', true);
        $re_password = $this->input->post('re_password', true);

        if ($email && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->message('Email không hợp lệ');
        }
        if($phone && !preg_match( '/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i', $phone )){
            return $this->message('Số điện thoại không hợp lệ');
        }
        $request = $this->input->post(null, true);
        if($request){
            foreach($request as $field=>$val){
                if(!$val) continue;
                $data[$field] = $this->input->post($field, true);
            }
        }

        if(!$uid){
            if($username){
                $username_special = url_friendly($username, '_');
                if($username_special!==$username) return $this->message('User name just allowed a-z A-Z 0-9');
                $check_username = $this->user_model->find_by('username', $username);
                if($check_username) return $this->message('User name already exist');
                if(!$password) return $this->message('Please input your password');
                if(!$re_password || $password!==$re_password) return $this->message('Re-type password does not match');
                $data = $this->pre_save($data);
            }

            if(isset($data['msg'])) return $data;
            $res = $this->user_model->add($data);
            if(!$res) return $this->message('Error. Please try again');
            return $this->message('Added successful.', true);
        }else{
            if($password){
                if($re_password!=$password){
                    return $this->message('Retype password is incorrect');
                }
            }
            $data = $this->pre_save($data);
            if(isset($data['msg'])) return $data;
            $res = $this->user_model->update($uid, $data);
            if(!$res) return $this->message('Error. Please try again');
            return $this->message('Saved successful.', true);
        }
    }
    private function pre_save($data){
        if(isset($data['password'])){
            $data['password'] = hash('sha512', $data['password']);
        }
        if(isset($data['re_password'])){
            unset($data['re_password']);
        }
        if(isset($data['id']) && !$data['id']){
            unset($data['id']);
        }
        return $data;
    }

    public function del($id){
        $confirm = $this->input->get('confirm', true);
        if(!$confirm){
            $data = array(
                'redirect' => base_url('user'),
                'del' => base_url('user/del/'.$id.'?confirm=1')
            );
            echo '<script>'.
                'var r = confirm("Are you sure to delete?");'.
                'if(r==true){'.
                'window.location.href = "'.$data['del'].'";'.
                '}else{'.
                'window.location.href = "'.$data['redirect'].'";}</script>';
            exit();
        }else{
            if(is_numeric($id)===true || $id>0){
                $this->user_model->delete($id);
            }
            redirect('user');
        }
    }
}
