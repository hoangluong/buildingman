<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <a class="btn btn-success" href="<?php echo base_url('user/add');?>">Thêm</a>

                <div class="box-tools pull-right">
                    <form action="" method="get" class="primary">
                        <div class="input-group" style="width: 250px;">
                            <input type="text" name="s" class="form-control input-md pull-right" placeholder="Tìm kiếm">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-md btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table">
                    <tbody>
                    <tr>
                        <th style="width: 20px">ID</th>
                        <th>Full Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Username</th>
                        <th>Role</th>
                        <th style="width: 125px">Action</th>
                    </tr>
                    <?php if(isset($data) && !empty($data)){
                        foreach($data as $item){?>
                    <tr>
                        <td style="width: 20px"><?php echo $item->id;?></td>
                        <td><?php echo $item->name;?></td>
                        <td><?php echo $item->phone;?></td>
                        <td><?php echo $item->email;?></td>
                        <td><?php echo $item->username;?></td>
                        <td><?php if($item->role==1){echo 'Administrator';}else{echo 'Member';};?></td>
                        <td style="width: 125px">
                            <a href="<?php echo base_url('/user/edit/'.$item->id);?>" class="label label-success">Edit</a>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <a href="<?php echo base_url('/user/del/'.$item->id);?>" class="label label-danger delete">Del</a>
                        </td>
                    </tr>
                        <?php }
                    }?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <?php if(isset($total_page) && $total_page>1){
                    vcc_paging($total_page, 'user/', 'page');
                }?>
            </div>
        </div><!-- /.box -->

    </div><!-- /.col -->
</div>