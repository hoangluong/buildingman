<?php

class Building_model extends Base_model
{

    protected $table = 'Buildings';

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_cond($cond)
    {
        if (isset($cond['s']) && $cond['s']) {
            $this->db->group_start();
            $this->db->or_like('name', $cond['s']);
            $this->db->group_end();
            unset($cond['s']);
        }
        return $cond;
    }

    function find($cond, $page, $per_page, $order = array())
    {
        $cond = $this->get_cond($cond);
        return parent::find($cond, $page, $per_page, $order);
    }

    function add($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    function edit($id, $data)
    {
        $this->db->where('id', $id)->update($this->table, $data);
        return $this->db->insert_id();
    }

    public function get($id)
    {
        $id = intval($id);
        $this->db->select('B.*, U.username, BO.name as owner_name, BO.phone as owner_phone, BO.email as owner_email, BO.address as owner_address, BO.website as owner_website, BO.description as owner_description');
        $this->db->from('Buildings B');
        $this->db->join('user_serving U', 'U.id = B.user_id', 'left');
        $this->db->join('building_owner BO', 'BO.id = B.building_owner_id', 'left');
        $this->db->where('B.id', $id);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return (isset($return[0])) ? $return[0] : null;
    }

    public function getAllBuilding()
    {
        $this->db->select('B.*, BO.name as owner_name, BO.phone as owner_phone, BO.email as owner_email, BO.address as owner_address, BO.website as owner_website, BO.description as owner_description');
        $this->db->from('Buildings B');

        $this->db->join('building_owner BO', 'BO.id = B.building_owner_id', 'left');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return (isset($return[0])) ? $return : null;
    }

    /**
     * @return string
     */
    public function get_tenant_building($building_id)
    {
        $id = intval($building_id);
        $this->db->select('bt.*, b.name as building_name, t.name as tenant_name, t.phone as tenant_phone, t.email as tenant_email');
        $this->db->from('building_tenants bt');
        $this->db->join('Buildings b', 'b.id = bt.building_id', 'left');
        $this->db->join('tenants t', 't.id = bt.tenant_id', 'left');
        $this->db->where('bt.building_id', $id);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return (isset($return[0])) ? $return : null;
    }

    public function get_invoice($building_id)
    {
        $this->db->select('*')
            ->from('invoices I')
            ->where('I.building_id', $building_id);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return (isset($return[0])) ? $return : null;
    }

    public function insertInvoice($data)
    {
        $this->db->insert('invoices', $data);
        return $this->db->insert_id();
    }

    public function insertBuildingOwner($data)
    {
        $this->db->insert('building_owner', $data);
        return $this->db->insert_id();
    }

    public function getAllOwners()
    {
        $this->db->select('BO.*, count(B.id) as building_number')
            ->from('building_owner BO')
            ->join('Buildings B', ' B.building_owner_id = BO.id', 'left');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return (isset($return)) ? $return : null;
    }

    public function getAllOwners2()
    {
        $this->db->select('BO.*')
            ->from('building_owner BO');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return (isset($return)) ? $return : null;
    }

    public function getMaxOwner()
    {
        $this->db->select('count(*) as max')
            ->from('building_owner');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return (isset($return[0])) ? $return[0] : null;
    }

    public function getOwner($id)
    {
        $this->db->select('BO.*')
            ->from('building_owner BO')
            ->where('BO.id', $id);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return (isset($return[0])) ? $return[0] : null;


    }

    public function insertUnit($data)
    {
        $this->db->insert('units', $data);
        return $this->db->insert_id();
    }
    public function getUnitsByBuilding($buildingid){
        $this->db->select('BO.*')
            ->from('units BO')
            ->where('BO.building_id', $buildingid);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return (isset($return[0])) ? $return[0] : null;
    }
    public function updateUnit($id,$data){
        $this->db->where('id', $id)->update('units', $data);
        return $this->db->insert_id();
    }
}