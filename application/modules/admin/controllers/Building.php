<?php

class Building extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('auth/user_model');
        $this->load->model('building/Building_model');

    }

    function index($page = 1)
    {

        $data = array();
        $per_page = 10;
        $data['page'] = $page;
        $data['pre'] = $page - 1;
        $data['next'] = $page + 1;
        $data['max'] = $max = count($this->Building_model->find(null, 1, 500000, array()));
        $total_rows = $max;
        $data['buildings'] = $this->Building_model->getAllBuilding();
        $data['total_page'] = ceil($total_rows / $per_page);
        $data['page_title'] = 'Buildings';
        $data['title'] = 'Buildings';
        $data['template'] = 'building/building_index_view';
        $this->load->view('layout', $data);
    }

    function create()
    {
        $data = array();
        $data['page_title'] = 'Create new building';
        $data['title'] = 'Create new building';
        $data['owners'] = $this->Building_model->getAllOwners();

        $data['template'] = 'building/building_create_view';
        $this->load->view('layout', $data);
    }

    function createStep2($id = 0)
    {
        $data = array();
        $data['page_title'] = 'Create new building';
        $data['title'] = 'Create new building';
        if ($id > 0) {
            $info = $this->Building_model->get($id);
            $data['building'] = $info;
            $data['units'] = $this->Building_model->getUnitsByBuilding($id);

        }


        $data['template'] = 'building/building_create_step2_view';
        $this->load->view('layout', $data);
    }

    function ajax_submit_building()
    {
        $user_id = $this->session->userdata('admin_id');
        if (Common::is_ajax()) {
            $arr = array();
            // Insert Owner
            $building_owner_id = (int)$this->input->post('building_owner_id', true);
            $arr['building_owner_id'] = $building_owner_id;
            // end Owner

            $name = $this->input->post('name', true);
            if ($name != '') $arr['name'] = $name;

            $company_name = $this->input->post('company_name', true);
            if ($company_name != '') $arr['company_name'] = $company_name;

            $address_street = $this->input->post('address_street', true);
            if (isset($address_street)) $arr['address_street'] = $address_street;
            $address_street_2 = $this->input->post('address_street', true);
            if (isset($address_street_2)) $arr['address_street'] = $address_street_2;
            $address_street_3 = $this->input->post('address_street_2', true);
            if (isset($address_street_3)) $arr['address_street_3'] = $address_street_3;
            $address_city = $this->input->post('address_city', true);
            if (isset($address_city)) $arr['address_city'] = $address_city;
            $country = $this->input->post('country', true);
            if (isset($country)) $arr['country'] = $country;
            $address_state = $this->input->post('address_state', true);
            if (isset($address_state)) $arr['address_state'] = $address_state;
            $bank_account = $this->input->post('bank_account', true);
            if (isset($bank_account)) $arr['bank_account'] = $bank_account;
            $address_postcode = $this->input->post('address_postcode', true);
            if (isset($address_postcode)) $arr['address_postcode'] = $address_postcode;
            $bank_account_reverse = $this->input->post('bank_account_reverse', true);
            if (isset($bank_account_reverse)) $arr['bank_account_reverse'] = $bank_account_reverse;
            $id = (int)$this->input->post('id', true);
            if ($id > 0) {
                $this->Building_model->edit($id, $arr);
            } else {
                $building_id = $this->Building_model->add($arr);
                if ($building_id) {
                    echo $number_unit = (int)$this->input->post('number_unit', true);
                    if ($number_unit > 0) {
                        for ($i = 0; $i < $number_unit; $i++) {
                            $arrunit = array(
                                'building_id' => $building_id
                            );
                            $this->Building_model->insertUnit($arrunit);
                        }
                    }

                }
            }
            echo json_encode(array('status' => true));
        }
        exit();
    }


    function edit($id = 0)
    {
        $data = array();
        $data['page_title'] = 'Edit building';
        $data['title'] = 'Edit building';
        if ($id > 0) {
            $info = $this->Building_model->get($id);
            $data['building'] = $info;
        }
        $data['owners'] = $this->Building_model->getAllOwners2();
        $data['template'] = 'building/building_edit_view';
        $this->load->view('layout', $data);
    }

    function detail($id = 0)
    {

        $data = array();
        $info = $this->Building_model->get($id);
        if (isset($info['id'])) {
            $invoices = $this->Building_model->get_invoice($id);
            $buildingTenant = $this->Building_model->get_tenant_building($id);
            $data['buildingTenant'] = $buildingTenant;
            $data['invoices'] = $invoices;

        }

        $data['building'] = $info;
        $data['page_title'] = $info['name'];
        $data['title'] = 'Building Information';


        $data['template'] = 'building/building_detail_view';
        $this->load->view('layout', $data);
    }

    function ajax_submit_invoice()
    {
        if (Common::is_ajax()) {
            $arr = array();
            $building_id = $this->input->post('building_id', true);
            $arr['building_id'] = $building_id;
            $account = $this->input->post('account', true);
            if (isset($account)) $arr['account'] = $account;
            $type = $this->input->post('type', true);
            if (isset($type)) $arr['type'] = $type;
            $payment_method = $this->input->post('payment_method', true);
            if (isset($payment_method)) $arr['payment_method'] = $payment_method;
            $check_number = $this->input->post('check_number', true);
            if (isset($check_number)) $arr['check_number'] = $check_number;
            $credit_action = $this->input->post('credit_action', true);
            if (isset($credit_action)) $arr['credit_action'] = $credit_action;
            $memo = $this->input->post('memo', true);
            if (isset($memo)) $arr['memo'] = $memo;
            $amount = $this->input->post('amount', true);
            if (isset($amount)) {
                if (in_array($type, array(1, 2)))
                    $arr['increase'] = $amount;
                else
                    $arr['decrease'] = $amount;
            }
            $arr['date'] = date('Y-m-d');
            $this->Building_model->insertInvoice($arr);
            echo json_encode(array('status' => true));
            die;
        }
    }

    function owner($page = 1)
    {
        $data = array();
        $per_page = 10;
        $data['page'] = $page;
        $data['pre'] = $page - 1;
        $data['next'] = $page + 1;
        $max = $this->Building_model->getMaxOwner();
        $data['max'] = $max['max'];

        $total_rows = $max['max'];;
        $data['owners'] = $this->Building_model->getAllOwners();

        $data['total_page'] = ceil($total_rows / $per_page);
        $data['page_title'] = 'Building';
        $data['title'] = 'Buildings';
        $data['template'] = 'building/owner_index_view';
        $this->load->view('layout', $data);
    }

    function ajax_get_info_owner($id = 0)
    {
        if (Common::is_ajax()) {
            $id = (int)$this->input->get('id', true);
            if ($id > 0) {
                $data = $this->Building_model->getOwner($id);
                echo json_encode(array('status' => true, 'data' => $data));
                die;
            } else {
                echo json_encode(array('status' => false));
            }
        } else {
            echo json_encode(array('status' => false, 'message' => 'not ajax'));
            die;
        }
    }

    function ajax_update_unit()
    {
        if (Common::is_ajax()) {
            $id = (int)$this->input->get('id', true);
            if ($id > 0) {
                $arr = array();
                $unit_number = (int)$this->input->get('unit_number', true);
                if ($unit_number) $arr['number'] = $unit_number;
                $baths = (int)$this->input->get('baths', true);
                if ($baths) $arr['baths'] = $baths;
                $beds = (int)$this->input->get('beds', true);
                if ($beds) $arr['beds'] = $beds;
                $sqft = $this->input->get('sqft', true);
                if ($sqft) $arr['sqft'] = $sqft;
                $this->Building_model->updateUnit($id, $arr);
                echo json_encode(array('status' => true));
                die;
            } else {
                echo json_encode(array('status' => false));
                die;
            }
        } else {
            echo json_encode(array('status' => false, 'message' => 'not ajax'));
            die;
        }
    }


}