<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends Front_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('auth/user_model');
    }

    private function redirect_back(){
        $back = $this->input->get('back', true);
        if($back){
            redirect(urldecode($back));
        }else{
            redirect('admin');
        }
    }

    function index(){
        if($this->session->userdata('admin_id')) $this->redirect_back();
        $data = array();
        $data['message'] = $this->message('Please input your account name and password !');
        if($_POST){
            $data['message'] = $this->login();
        }

        $this->load->view('login/login_view', $data);
    }

    private function login(){
        $u_acc = $this->input->post('u_acc', true);
        $u_pwd = $this->input->post('u_pwd', true);
        $back = $this->input->get('back', true);
        $count = $this->session->userdata('login_count');
		 
        $count = unserialize($count);
        if(empty($count)){
            $count = array(
                'time' => time(),
                'submit' => 1
            );
            $this->session->set_userdata('login_count', serialize($count));
        }
      //  if($count['submit'] > 10){
      //      $min_left = 10 * 60 * 60 - (time() - $count['time']);
      //      return $this->message("Xin vui lòng thử lại trong {$min_left} phút, vui lòng thử lại !");
      //  }
        if ($count['time'] + 10 * 60 * 60 > time()) {
            $this->session->unset_userdata('login_count');
        }
        $count['submit'] = $count['submit'] + 1;
        $this->session->set_userdata('login_count', serialize($count));

        if(!$u_acc || !$u_pwd) return $this->message('Please input your account name and password !');

        //check captcha
        $captchaResponse = trim($this->input->post('g-recaptcha-response'));
    //    $checkCaptcha = $this->captcha_http_post($captchaResponse);
      //  if(!$checkCaptcha || !isset($checkCaptcha->success) || $checkCaptcha->success != 1){
          //   return $this->message('Bạn chưa nhập đúng Captcha, vui lòng thử lại !');
     //   }

        $user_data = $this->user_model->checkLogin($u_acc, $u_pwd);
	 
        if(empty($user_data)){
            return $this->message('Your account or password are incorrect, please try again !');
        }
        if(!$user_data->status) return $this->message('Your account has not actived, please contact to administrator');
        $this->session->unset_userdata('login_count');
        $this->session->set_userdata('admin_id', $user_data->id);
        $this->session->set_userdata('user_data', $user_data);
        
        //success
        $this->redirect_back();
        return $this->message('Success', true);
    }

    private function captcha_http_post($captchaResponse)
    {
        $url = "https://www.google.com/recaptcha/api/siteverify";
        $secret = '6LcYTiETAAAAAG0svIZRauqd_LeEujhn4Q1Ytdny';
        $post_params = "secret=" . $secret . "&response=" . $captchaResponse;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        if (!empty($post_params)) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
        }
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }

    public function logout(){
        $this->session->unset_userdata('admin_id');
        $this->session->unset_userdata('user_data');
        redirect(site_url('admin/login'));
    }
}