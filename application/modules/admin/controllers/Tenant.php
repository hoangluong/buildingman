<?php

class Tenant extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('auth/user_model');
        $this->load->model('tenant/Tenant_model');

    }

    function index($page = 1)
    {

        $data = array();
        $per_page = 10;
        $data['page'] = $page;
        $data['pre'] = $page - 1;
        $data['next'] = $page + 1;
        $data['max'] = $max = @count($this->Tenant_model->find(null, 1, 500000, array()));
        $total_rows = $max;
        $data['tenants'] = $this->Tenant_model->find(null, $page, $per_page, array());


        $data['total_page'] = ceil($total_rows / $per_page);

        $data['page_title'] = 'Tenants';
        $data['title'] = 'Tenants';
        $data['template'] = 'tenant/tenants_index_view';
        $this->load->view('layout', $data);
    }

    function create()
    {
        $data = array();
        $data['page_title'] = 'Create new Tenant';
        $data['title'] = 'Create new Tenant';
        $data['template'] = 'tenant/tenants_create_view';
        $this->load->view('layout', $data);
    }

    function ajax_submit_tenant()
    {
        $user_id = $this->session->userdata('admin_id');

        if(Common::is_ajax() == true){
            $building_id = $this->input->post('building_id', true);
            $name = $this->input->post('name', true);
            $company_name = $this->input->post('company_name', true);
            $address_1 = $this->input->post('address_line_1', true);
            $address_2 = $this->input->post('address_line_2', true);
            $attention = $this->input->post('attention', true);
            $country = $this->input->post('country', true);
            $city = $this->input->post('city', true);
            $province = $this->input->post('province', true);
            $postcode = $this->input->post('postcode', true);
            $phone = $this->input->post('phone', true);
            $email = $this->input->post('email', true);
            $area = $this->input->post('area', true);
            $space_type = $this->input->post('space_type', true);
            $gross_area = $this->input->post('gross_area', true);
            $lease_area = $this->input->post('lease_area', true);
            $certified_area = $this->input->post('certified_area', true);
            $lease_date_start = $this->input->post('lease_date_start', true);
            $lease_date_ends = $this->input->post('lease_date_ends', true);
            $arr = array(
                'name' => $name,
                'company_name' => $company_name,
                'address_line_1' => $address_1,
                'address_line_2' => $address_2,
                'attention' => $attention,
                'country' => $country,
                'city' => $city,
                'province' => $province,
                'postcode' => $postcode,
                'phone' => $phone,
                'email' => $email,
                'space_type' => $space_type,
                'gross_area' => $gross_area,
                'lease_area' => $lease_area,
                'certified_area' => $certified_area,
             //   'user_id' => $user_id,
                'lease_date_start' => $lease_date_start,
                'lease_date_ends' => $lease_date_ends,
                'created_at' =>date('Y-m-d H:i:s')
            );
            $tenant_id = $this->Tenant_model->add($arr);
            if($tenant_id && $building_id){
                $bt_arr = array(
                    'building_id' =>$building_id,
                    'tenant_id' => $tenant_id,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->Tenant_model->add_building_tenant($bt_arr);
            }
            echo json_encode(array('status' => true));
        }
        exit();
    }
}
