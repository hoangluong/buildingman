<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $temp = array();
        $temp['page_title'] = 'Dashboard';
        $temp['template'] = 'dashboard/dashboard';
        $this->load->view('layout', $temp);
    }

  

}