<?php

class Rentroll extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('auth/user_model');
        $this->load->model('building/Building_model');
        $this->load->model('rentroll/Rentroll_model');
    }

    function index($page = 1){
        $data = array();
        $per_page = 10;
        $data['page'] = $page;
        $data['pre'] = $page - 1;
        $data['next'] = $page + 1;
        $data['max'] = $max = @count($this->Rentroll_model->find(null, 1, 500000, array()));
        $total_rows = $max;
        $data['rentrolls'] = $this->Rentroll_model->getAllRentroll();


        $data['total_page'] = ceil($total_rows / $per_page);

        $data['page_title'] = 'Rent roll';
        $data['title'] = 'Rent roll';
        $data['template'] = 'rentroll/rentroll_index_view';
        $this->load->view('layout', $data);
    }
}
