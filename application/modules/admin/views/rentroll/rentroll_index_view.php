<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
    button.btn-primary {
        width: 80px;
    }

    .paddingtop18 {
        padding-top: 17px !important;
    }

    th {
        text-align: center;
    }

    .col-sm-6 input[type="radio"] {
        margin: 5px;

    }
</style>
<div>
    <section class="content-header">
        <h1><?php echo $page_title; ?></h1>
    </section>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="clearfix">
            <div class="pull-right">
                <a href="<?php echo base_url('admin/building/create') ?>" class="btn btn-info">Add lease</a>
            </div>
        </div>
    </div>
    <br>

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php echo '<b>' . $max . '</b> ' . $title ?> matchs
            </div>

            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Lease</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Type</th>
                            <th>Day left</th>
                            <th class="text-center">Rent</th>
                            <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                         //print_r($rentrolls);
                        if (isset($rentrolls) && is_array($rentrolls)) {
                            foreach ($rentrolls as $item) {
                                $item = (array)$item;
                                $i++;
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $item['building_address_street'].' ('.Common::building_type($item['building_type']).') '.' - '.$item['unit_number'];?></td>
                                    <td class="text-center">Active</td>
                                    <td class="text-left">
                                        <?php echo Common::rent_roll_type($item['type'])?><br>
                                        <i>(<?php echo date('m/d/Y',strtotime($item['start_date']));?> - <?php echo date('m/d/Y',strtotime($item['end_date']));?>)</i>
                                    </td>
                                    <td></td>
                                    <td class="text-center"><?php echo '$'.$item['rent'];?></td>
                                    <td></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"
                                 style="float: right;">
                                <ul class="pagination" style="margin: 0px;">

                                    <?php
                                    for ($i = 1; $i <= $total_page; $i++) {

                                        ?>

                                        <li class="paginate_button <?php echo ($page == $i) ? 'active' : ''; ?>"
                                            aria-controls="dataTables-example"
                                            tabindex="0">
                                            <a href="<?php echo site_url('admin/building/index/' . $i) ?>"><?php echo $i ?></a>
                                        </li>
                                        <?php


                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>


                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->

        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<style>
    .table-responsive {
        overflow-x: inherit;
    }
</style>