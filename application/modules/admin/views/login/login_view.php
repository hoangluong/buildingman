<?php $asset = base_url('public/admin/');
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Building Manager</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link href="<?php echo $asset . 'bootstrap/css/bootstrap.min.css'; ?>" rel="stylesheet">
    <link href="<?php echo $asset . 'dist/css/AdminLTE.min.css'; ?>" rel="stylesheet">
    <link href="<?php echo $asset . 'css/style.css'; ?>" rel="stylesheet">

    <script src="<?php echo $asset.'js/jquery-1.12.4.min.js';?>"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="/"><b>Login</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <div class="login-panel panel panel-default">
            <div class="panel-body">
                <?php if (isset($message) && $message) { ?>
                    <!-- alert alert-info alert-dismissible -->
                    <div style="display: block;"
                         class="alert <?php echo $message['success'] ? 'alert-info ' : 'alert-danger '; ?>alert-dismissible"
                         id="message">
                        <button type="button" id="btn-close-msg" class="close" data-dismiss="alert"
                                aria-hidden="true">×
                        </button>
                        <p id="message-content"><?php echo $message['msg']; ?></p>
                    </div>
                <?php } ?>
                <form id="form1" method="post" action="">
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="Username" name="u_acc" type="text" autofocus>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Password" name="u_pwd" type="password" value="">
                        </div>
                        <div class="g-recaptcha" data-sitekey="6LcYTiETAAAAALbl9jX4I-mwmLCONZATbhWhJZ8m"></div>
                        <input type="submit" class="btn btn-lg btn-success btn-block" value="Login"/>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
</body>
</html>


