<form role="form" id="createInvoice" action="" method="post" autocomplete="off">
    <div class="box-body">
        <input type="hidden" value="<?php echo isset($id) ? $id : 0 ?>" name="building_id">
        <div class="row-1 clearfix">
            <div class="col-sm-6">

                <div class="form-group">
                    <label for="name">Bank Account <span class="text-red">*</span></label>
                    <select name="account" id="account" class="form-control">
                        <?php
                        $lstType = Common::bank_account();
                        print_r($lstType);
                        foreach ($lstType as $item => $val) {
                            ?>
                            <option value="<?php echo $val ?>"><?php echo $val ?></option>

                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-6">

            </div>
        </div>
        <div class="row-1 clearfix">
            <div class="col-sm-6">

                <div class="form-group">
                    <label for="name">Type <span class="text-red">*</span></label>
                    <select name="type" id="type" class="form-control">
                        <?php
                        $lstType = Common::payment_type();
                        print_r($lstType);
                        foreach ($lstType as $item => $val) {
                            ?>
                            <option value="<?php echo $item ?>"><?php echo $val ?></option>

                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-6">

            </div>
        </div>
        <div class="row-1 clearfix">
            <div class="col-sm-6">

                <div class="form-group">
                    <label for="name">Payment Method <span class="text-red">*</span></label>
                    <select name="payment_method" id="payment_method" class="form-control">
                        <?php
                        $lstType = Common::payment_method();
                        print_r($lstType);
                        foreach ($lstType as $item => $val) {
                            ?>
                            <option value="<?php echo $item ?>"><?php echo $val ?></option>

                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="email" class="clearfix" style="display: block">Check Number</label>
                    <div class="col-xs-5">
                        <div class="row">
                            <input type="text" class="form-control col-sm-4" id="check_number" name="check_number"
                                   placeholder="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-1 clearfix">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="email">Amount</label>
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" name="amount" id="amount" class="form-control">
                        <span class="input-group-addon">.00</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-1 clearfix">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="email">Credit Action</label>
                    <textarea class="form-control" name="credit_action" id="credit_action" rows="3"></textarea>
                </div>
            </div>
        </div>
        <div class="row-1 clearfix">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="email">Memo</label>
                    <textarea class="form-control" name="memo" id="memo" rows="3"></textarea>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
<style>
    #modal-invoice .modal-dialog {
        width: 600px;
        margin: 5% auto auto;
    }

    .modal-header {
        border-top: 3px #3c8dbc solid;
    }

    .modal-title {
        font-size: 16px;
        font-weight: bold;
        text-transform: uppercase;
    }
</style>
<script>
    $(document).ready(function () {
        $('form#createInvoice').submit(function (event) {
            var formData = $(this).serialize();
            // process the form
            $.ajax({
                type: 'POST',
                url: base_url + '/admin/building/ajax_submit_invoice',
                data: formData,
                dataType: 'json',
                encode: true
            })
                .success(function (data) {
                   window.location.reload();
                });
            event.preventDefault();
        });

    });



</script>