<!--Modal Invoince-->
<div class="modal fade" id="modal-invoice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Invoice</h4>
            </div>
            <div class="modal-body">
                <?php $this->load->view('building_popup_add_invoice', array('id' => $building['id'])); ?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="building-detail">
    <div class="col-sm-12">
        <h1><?php echo $building['address_street']; ?></h1>

        <div class="sub-title"><?php echo ($building['type'])?Common::building_type($building['type']):''; ?></div>
    </div>
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#information" data-toggle="tab" aria-expanded="true">Summary</a></li>
                <li><a href="#financial" data-toggle="tab" aria-expanded="true">Financials</a></li>
                <li><a href="#units" data-toggle="tab" aria-expanded="true">Units</a></li>
                <li><a href="#task" data-toggle="tab" aria-expanded="true">Tasks</a></li>
                <li><a href="#event_history" data-toggle="tab" aria-expanded="true">Event History</a></li>
                <li><a href="#vendor" data-toggle="tab" aria-expanded="true">Vendor</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="information">
                    <div class="col-sm-9">
                        <div class="block clearfix">

                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="building-thumb">
                                        <img src="<?php echo base_url('public/images/building.jpg') ?>"
                                             alt=""
                                             style="width: 100%">
                                    </div>
                                </div>
                                <div class="col-xs-8">

                                    <div class="block-title">Building details</div>
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <label for="">Address</label>

                                            <div><?php echo $building['address_street']; ?></div>
                                            <div><?php echo $building['address_street_2']; ?></div>
                                            <div><?php echo $building['address_street_3']; ?></div>
                                        </div>
                                        <div class="row">
                                            <label for="">Rental Owners</label>

                                            <div><?php echo $building['owner_name']; ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="">Operating Account</label>

                                                    <div><?php echo Common::bank_account($building['bank_account']); ?></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="">Property reverse</label>

                                                    <div>
                                                        $<?php echo number_format($building['bank_account_reverse']); ?>
                                                        .00
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="cash-info alert alert-success">
                            <ul>
                                <li><label class="first" for="">Cash balance:</label> N/A</li>
                                <li><label for=""> - Security deposits and early payments:</label>N/A</li>
                                <li><label for=""> - Property reserve:</label>
                                    $<?php echo number_format($building['bank_account_reverse']); ?></li>
                                <li>
                                    <div class="underline-dot">
                                        <hr>
                                    </div>
                                </li>
                                <li><label for="">Available</label> N/A</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="financial">
                    <div>
                        <div class="col-sm-3">
                            <div class="row">
                                <select name="" id="" class="form-control">
                                    <option value="">Three months to date</option>
                                </select></div>
                        </div>

                        <table class="table table-condensed financial-table" style="margin-top: 20px;">
                            <thead>
                            <tr class="ccc">
                                <th style="width: 50%;">PROPERTY ACCOUNT</th>
                                <th><?php echo date('M Y', strtotime('-2 months')) ?></th>
                                <th><?php echo date('M Y', strtotime('-1 months')) ?></th>
                                <th><?php echo date('M Y') ?></th>
                                <th><?php echo 'Total as of ' . date('m/d/Y') ?></th>
                                <th></th>
                            </tr>
                            <tr>
                                <th colspan="6">Income</th>
                            </tr>
                            <tr>
                                <td>Rent Income</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Total income</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th colspan="6">Expenses</th>
                            </tr>
                            <tr>
                                <td>Landscaping	</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Repairs</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Supplies</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Utilities</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Total expenses	</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Net operating income</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            <tr>
                                <th>Net income	</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="tab-pane" id="units">
                    <div>updating...</div>
                </div>
                <div class="tab-pane" id="task">
                    <div>updating...</div>
                </div>
                <div class="tab-pane" id="event_history">
                    <div>updating...</div>
                </div>
                <div class="tab-pane" id="vendor">
                    <div>updating...</div>
                </div>

            </div>
            <!-- /.tab-content -->
        </div>

        <!-- /.nav-tabs-custom -->
    </div>
</div>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Tenant</h4>
            </div>
            <div class="modal-body">
                <?php $this->load->view('building_popup_add_tenant', array('id' => $building['id'])); ?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<script>
    $('.editBuildingContact').click(function () {
        $('.info').toggle('slow');
        $('.edit-form-contact').toggle('slow');
    });
    $('.editBuildingDetail').click(function () {
        $('.info-details').toggle('slow');
        $('.edit-form-details').toggle('slow');
    });

    $(document).ready(function () {
        $('form#formEditBuildingDetail, form#formEditBuildingInfo').submit(function (event) {
            var formData = $(this).serialize();
            // process the form
            $.ajax({
                type: 'POST',
                url: base_url + '/admin/building/ajax_submit_building',
                data: formData,
                dataType: 'json',
                encode: true
            })
                .success(function (data) {
                    window.location.reload();
                });
            event.preventDefault();
        });

    });
</script>

<style>
    .edit-form-contact, .edit-form-details {
        border-left: 5px #3c8dbc80 solid;
        background: #f8f8f8;
    }

    .edit-form table {
        margin-bottom: 0;
    }

    .block h4 {
        font-size: 16px;
        font-weight: bold;
    }

    .block h4 a {
        padding-left: 10px;
        font-size: 12px;
        font-weight: normal;
    }

    .block .table tr th {
        font-weight: normal;
        color: #868686;
    }

    .nav-tabs-custom > .nav-tabs > li:first-of-type.active > a {
        font-weight: bold;
    }
</style>