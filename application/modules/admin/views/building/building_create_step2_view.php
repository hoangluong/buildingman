<script type="text/javascript">
    $(document).ready(function () {
        $('#dateadd').datepicker({dateFormat: 'yy-mm-dd'});
    });

</script>


<div class="col-sm-12">
    <h1><?php echo $building['address_street']; ?></h1>

    <div class="sub-title"><?php echo ($building['type']) ? Common::building_type($building['type']) : ''; ?></div>
</div>
<form role="form" id="create" action="" method="post">
    <div class="col-lg-12">
        <div class="row">
            <div class="box box-primary  clearfix">
                <div class="box-header with-border">
                    <h3 class="box-title">Units</h3>
                </div>
                <div class="block clearfix">
                    <div class="unit-form">
                        <?php
                        foreach ($units as $item) {
                            ?>
                            <div class="unit-item clearfix">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <ul>
                                            <li class="col-sm-2">
                                                <div for="">Unit Number</div>
                                                <input type="text" data-id="" class="form-control">
                                            </li>
                                            <li class="col-sm-6">
                                                <div for="">Unit Address</div>
                                                <?php echo $building['address_street'] ?>-c
                                            </li>
                                            <li class="col-sm-1">
                                                <div>Room</div>
                                                <select name="" id="" class="form-control"
                                                        style="width: auto !important;">
                                                    <option value="">Beds</option>
                                                </select>
                                            </li>
                                            <li class="col-sm-1">
                                                <div>&nbsp;</div>
                                                <select name="" id="" class="form-control"
                                                        style="width: auto !important;">
                                                    <option value="">Baths</option>
                                                </select>
                                            </li>
                                            <li class="col-sm-2">
                                                <div>SQFT</div>
                                                <input type="text" class="form-control">
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<style>
    h4 {
        font-size: 14px;
        padding-left: 20px;
        font-weight: bold;
        background: #3c8dbc1c;
        padding: 10px;
    }

    h2 {
        font-size: 14px;
        margin-top: 0;
        font-weight: bold;
        padding: 10px;
        padding-left: 0px;
    }

    .block {
        padding: 20px;
    }

    .form-group label {
        color: #999999;
    }
</style>
<script>
    $(document).ready(function () {

        var choose = ($('.owner_chooose').val());
        if (choose == 2) {
            $('#building_owner_id').removeAttr('disabled');
        }

        $('#countUnit').blur(function () {
            var num = $(this).val();
            var html = '';
            for (var i = 0; i < num; i++) {
                html += '';
            }
        });

        function updateUnit() {
            $.ajax({
                type: 'POST',
                url: base_url + '/admin/building/ajax_update_unit',
                data: {id:},
                dataType: 'json',
                encode: true
            }).success(function (data) {
                window.location.href = (base_url + 'admin/building');
            });
        }


    });

</script>
