<script type="text/javascript">
    $(document).ready(function () {
        $('#dateadd').datepicker({dateFormat: 'yy-mm-dd'});
    });

</script>
<form role="form" id="create" action="" method="post">

    <div class="col-lg-12">
        <div class="row">
            <div class="box box-primary  clearfix">
                <div class="box-header with-border">
                    <h3 class="box-title">Add new Building</h3>
                </div>
                <div class="block clearfix">
                    <h2>What is the building type?</h2>
                    <div class="col-sm-3">
                        <div class="row">
                            <select name="type" id="type" class="form-control">
                                <?php
                                $building_type = Common::building_type();
                                var_dump($building_type);
                                foreach ($building_type as $key => $val) {
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $val ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="block clearfix">
                    <h2>What is the street address?</h2>
                    <div class="form-group col-sm-6">
                        <div class="row">
                            <label for="exampleInputEmail1">Street Address</label>
                            <input type="text" name="address_street" class="form-control" id="address_street" placeholder=""><br>
                            <input type="text" name="address_street_2" class="form-control" id="address_street_2" placeholder=""><br>
                            <input type="text" name="address_street_3" class="form-control" id="address_street_3" placeholder="">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="row">
                                <div class="form-group col-sm-4">
                                    <label for="exampleInputEmail1">City</label>
                                    <input type="text" name="address_city" class="form-control" id="address_city" placeholder="City">
                                </div>

                                <div class="form-group col-sm-4">
                                    <label for="exampleInputEmail1">State</label>
                                    <input type="text" name="address_state" class="form-control" id="address_state" placeholder="State">
                                </div>
                                <div class="form-group col-sm-4" style="margin-right: -15px;">
                                    <label for="exampleInputEmail1">Postal Code</label>
                                    <input type="text" name="address_postcode" class="form-control" id="address_postcode" placeholder="Postal Code">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-sm-6">
                        <div class="row">
                            <label for="exampleInputEmail1">Country</label>
                            <input type="text" class="form-control" id="country" placeholder="Country">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                </div>
                <div class="block clearfix">
                    <h2>Who is the building owner?</h2>
                    <div class="col-sm-6">
                        <div class="row">
                            <select name="building_owner_id" id="building_owner_id" class="form-control">
                                <?php
                                if (isset($owners) && is_array($owners)) {
                                    foreach ($owners as $item) {
                                        ?>
                                        <option value="<?php echo $item['id']; ?>"><?php echo $item['name'] ?></option>
                                    <?php }
                                } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="block clearfix">
                    <h2>What is this property's primary bank account?</h2>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label for="exampleInputEmail1">Operating Account</label>
                                    <select name="bank_account" id="bank_account" class="form-control">
                                        <?php
                                        $bank_account = Common::bank_account();

                                        foreach ($bank_account as $key => $val) {
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $val ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="exampleInputEmail1">Building Reserve</label>
                                    <input type="text" name="bank_account_reverse" class="form-control" id="bank_account_reverse" value="0.00" placeholder="">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="unit-form block clearfix">
                    <h2>Enter Unit</h2>
                    <div class="form-group col-sm-6">
                        <div class="row">
                            <label for="exampleInputEmail1">Number unit</label>
                            <input type="number" class="form-control" name="number_unit" value="1" style="width: 100px;" id="countUnit"
                                   placeholder="">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</form>
<style>
    h4 {
        font-size: 14px;
        padding-left: 20px;
        font-weight: bold;
        background: #3c8dbc1c;
        padding: 10px;
    }

    h2 {
        font-size: 14px;
        margin-top: 0;
        font-weight: bold;
        padding: 10px;
        padding-left: 0px;
    }

    .block {
        padding: 20px;
    }

    .form-group label {
        color: #999999;
    }
</style>
<script>
    $(document).ready(function () {

        var choose = ($('.owner_chooose').val());
        if (choose == 2) {
            $('#building_owner_id').removeAttr('disabled');
        }

        $('#countUnit').blur(function () {
            var num = $(this).val();
            var html = '';
            for (var i = 0; i < num; i++) {
                html += '';
            }
        });

        $('form').submit(function (event) {

            var formData = $(this).serialize();
            // process the form
            $.ajax({
                type: 'POST',
                url: base_url + '/admin/building/ajax_submit_building',
                data: formData,
                dataType: 'json',
                encode: true
            }).success(function (data) {
        //        window.location.href = (base_url + 'admin/building');
            });

            event.preventDefault();
        });

    });

</script>
