<script type="text/javascript">
    $(document).ready(function () {
        $('#dateadd').datepicker({dateFormat: 'yy-mm-dd'});
    });

</script>
<?php
//print_r($owners);

?>
<form role="form" id="create" action="" method="post">
    <input type="hidden" id="id" name="id" value="<?php echo $building['id'];?>">
    <div class="col-sm-4">
        <div class="box box-primary  clearfix">
            <div class="box-header with-border">
                <h3 class="box-title">Owner building</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label style="width: 50%;">
                        <input type="radio" name="owner_chooose" value="1" class="flat-red owner_chooose">
                        Create new
                    </label>
                    <label>
                        <input type="radio" name="owner_chooose" value="2" class="flat-red owner_chooose" checked>
                        Select one
                    </label>

                </div>

                <div class="form-group">
                    <select id="building_owner_id" name="building_owner_id" class="form-control">
                        <option value="0">Select</option>
                        <?php

                        foreach ($owners as $item) {
                            ?>
                            <option value="<?php echo $item['id'] ?>" <?php echo ($item['id'] == $building['building_owner_id']) ? 'selected="selected"' : ''; ?>><?php echo $item['name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Name <span class="text-red">*</span></label>
                    <input type="text" class="form-control" name="owner_name" id="owner_name"
                           placeholder="Name">
                </div>
                <div class="form-group">
                    <label for="name">Phone <span class="text-red"></span></label>
                    <input type="text" class="form-control" name="owner_phone" id="owner_phone"
                           placeholder="Phone">
                </div>
                <div class="form-group">
                    <label for="name">Email <span class="text-red"></span></label>
                    <input type="text" class="form-control" name="owner_email" id="owner_email"
                           placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="name">Address <span class="text-red"></span></label>
                    <input type="text" class="form-control" name="owner_address" id="owner_address"
                           placeholder="Address">
                </div>
                <div class="form-group">
                    <label for="name">Website <span class="text-red"></span></label>
                    <input type="text" class="form-control" name="owner_website" id="owner_website"
                           placeholder="Website">
                </div>
                <div class="form-group">
                    <label for="name">Note <span class="text-red"></span></label>
                    <textarea class="form-control" name="owner_description" id="owner_description"
                              placeholder="Description"></textarea>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-8">
        <div class="row">
            <div class="box box-primary  clearfix">
                <div class="box-header with-border">
                    <h3 class="box-title">Information</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="block clearfix">
                    <h4>Building Contacts</h4>
                    <div class="info clearfix">
                        <div class="col-xs-6">

                            <div class="form-group">
                                <label for="name">Name <span class="text-red">*</span></label>
                                <input type="text" class="form-control" name="name" id="name"
                                       placeholder="Name" required value="<?php echo $building['name']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="text" class="form-control" id="phone" name="phone"
                                       placeholder="Phone" value="<?php echo $building['phone']; ?>">
                            </div>

                        </div>
                        <div class="col-xs-6">
                            <div class="table-responsive">
                                <div class="form-group">
                                    <label for="company_name">Company Name<span class="text-red">*</span></label>
                                    <input type="text" class="form-control" name="company_name" id="company_name"
                                           placeholder="Company Name" required
                                           value="<?php echo $building['company_name']; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" name="email"
                                           placeholder="Email" value="<?php echo $building['email']; ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="block clearfix">
                    <h4>Building details</h4>
                    <div class="info-details">
                        <div class="col-xs-6">
                            <div class="table-responsive">
                                <div class="form-group clearfix">
                                    <label for="country">Country</label>
                                    <input type="text" class="form-control col-xs-6" id="country" name="country"
                                           placeholder="Country" value="<?php echo $building['country']; ?>">
                                </div>
                                <div class="form-group clearfix">
                                    <label for="province">Province</label>
                                    <input type="text" class="form-control col-xs-6" id="province" name="province"
                                           placeholder="Province" value="<?php echo $building['province']; ?>">
                                </div>
                                <div class="form-group clearfix">
                                    <label for="exampleInputEmail1">Address 1</label>
                                    <input type="text" class="form-control col-xs-6" id="address_1" name="address_1"
                                           placeholder="Address 1" value="<?php echo $building['address_1']; ?>">
                                </div>
                            </div>
                            <div class="table-responsive">
                                <div class="form-group">
                                    <label for="area">Area</label>
                                    <input type="number" class="form-control" id="area" name="area"
                                           placeholder="Area" value="<?php echo $building['area']; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="office_area">Office Area</label>
                                    <input type="text" class="form-control" id="office_area" name="office_area"
                                           placeholder="Office Area" value="<?php echo $building['office_area']; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">


                            <div class="table-responsive">
                                <div class="form-group clearfix">
                                    <label for="city">City</label>
                                    <input type="text" class="form-control col-xs-6" id="city" name="city"
                                           placeholder="City" value="<?php echo $building['city']; ?>">
                                </div>
                                <div class="form-group clearfix">
                                    <label for="post_code">Post Code</label>
                                    <input type="text" class="form-control col-xs-6" id="post_code" name="post_code"
                                           placeholder="Post Code" value="<?php echo $building['postcode']; ?>">
                                </div>
                                <div class="form-group clearfix">
                                    <label for="exampleInputEmail1">Address 2</label>
                                    <input type="text" class="form-control col-xs-6" id="address_2" name="address_2"
                                           placeholder="Address 2" value="<?php echo $building['address_2']; ?>">
                                </div>
                            </div>
                            <div class="table-responsive">
                                <div class="form-group">
                                    <label for="other_area">Other Area</label>
                                    <input type="text" class="form-control" id="other_area" name="other_area"
                                           placeholder="Other Area" value="<?php echo $building['other_area']; ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</form>
<style>
    h4 {
        font-size: 14px;
        padding-left: 20px;
        font-weight: bold;
        background: #3c8dbc1c;
        padding: 10px;
    }
</style>
<script>
    $(document).ready(function () {

        var choose = ($('.owner_chooose').val());
        if (choose == 2) {
            $('#building_owner_id').removeAttr('disabled');
        }
        slOwner(<?php echo $building['building_owner_id'];?>);
        function slOwner(id) {
            $.ajax({
                url: base_url + 'admin/building/ajax_get_info_owner',
                type: 'GET',
                dataType: 'JSON',
                data: {id: id},
                success: function (response) {
                    $('#owner_name').attr('readonly', false).val('');
                    $('#owner_phone').attr('readonly', false).val('');
                    $('#owner_email').attr('readonly', false).val('');
                    $('#owner_address').attr('readonly', false).val('');
                    $('#owner_website').attr('readonly', false).val('');
                    $('#owner_description').attr('readonly', false).val('');
                    if (response.status == true) {
                        var d = response.data;
                        $('#owner_name').attr('readonly', true).val(d.name);
                        $('#owner_phone').attr('readonly', true).val(d.phone);
                        $('#owner_email').attr('readonly', true).val(d.email);
                        $('#owner_address').attr('readonly', true).val(d.address);
                        $('#owner_website').attr('readonly', true).val(d.website);
                        $('#owner_description').attr('readonly', true).val(d.description);
                    }
                }
            });
        }

        $('#building_owner_id').change(function () {
            slOwner(this.value);
        });

        $('form').submit(function (event) {

            var formData = $(this).serialize();
            // process the form
            $.ajax({
                type: 'POST',
                url: base_url + '/admin/building/ajax_submit_building',
                data: formData,
                dataType: 'json',
                encode: true
            }).success(function (data) {
                window.location.href = (base_url + 'admin/building');
            });

            event.preventDefault();
        });

    });

</script>
