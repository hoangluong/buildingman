 
<!--Modal Invoince-->
<div class="modal fade" id="modal-invoice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Invoice</h4>
            </div>
            <div class="modal-body">
                <?php $this->load->view('building_popup_add_invoice', array('id' => $building['id'])); ?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="col-md-3">
    <!-- Profile Image -->
    <div class="box box-primary">
        <div class="box-body box-profile">
            <img src="<?php echo base_url('public/images/building_icon.png') ?>" alt="" style="width: 100%">
            <h3 class="profile-username text-center"><?php echo $building['owner_name']; ?></h3>

            <p class="text-muted text-center">Owner</p>

            <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                    <b>Phone</b> <a class="pull-right"><?php echo $building['owner_phone'] ?></a>
                </li>
                <li class="list-group-item">
                    <b>Email</b> <a class="pull-right"><?php echo $building['owner_email'] ?></a>
                </li>
                <li class="list-group-item">
                    <b>Address</b> <a class="pull-right"><?php echo $building['owner_address'] ?></a>
                </li>
                <li class="list-group-item">
                    <b>Website</b> <a class="pull-right"><?php echo $building['owner_website'] ?></a>
                </li>
                <li class="list-group-item">
                    <b>Description</b> <a class="pull-right"><?php echo $building['owner_description'] ?></a>
                </li>

            </ul>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<div class="col-md-9">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#information" data-toggle="tab" aria-expanded="true">Information</a></li>
            <li><a href="#financial" data-toggle="tab" aria-expanded="true">Financials</a></li>
            <li><a href="#tenants" data-toggle="tab" aria-expanded="true">Tenants</a></li>
            <li><a href="#task" data-toggle="tab" aria-expanded="true">Tasks</a></li>
            <li><a href="#event_history" data-toggle="tab" aria-expanded="true">Event History</a></li>
            <li><a href="#vendor" data-toggle="tab" aria-expanded="true">Vendor</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="information">
                <div class="block clearfix">
                    <h4>Building Contacts <a href="javascript:void(0)" class="editBuildingContact">Edit</a></h4>
                    <div class="info clearfix">
                        <div class="col-xs-6">

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th style="width:50%">Name</th>
                                        <td><?php echo $building['name']; ?></td>
                                    </tr>
                                    <?php
                                    if ($building['phone']) {
                                        ?>
                                        <tr>
                                            <th style="width:50%">Phone</th>
                                            <td><?php echo $building['phone']; ?></td>
                                        </tr>
                                    <?php } ?>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="col-xs-6">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <?php
                                    if ($building['company_name']) {
                                        ?>
                                        <tr>
                                            <th style="width:50%">Company Name</th>
                                            <td><?php echo $building['company_name']; ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php
                                    if ($building['email']) {
                                        ?>
                                        <tr>
                                            <th style="width:50%">Email</th>
                                            <td><?php echo $building['email']; ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="edit-form-contact clearfix" style="display: none;">
                        <form id="formEditBuildingInfo" action="" method="post">
                            <input type="hidden" name="id" value="<?php echo $building['id']; ?>">
                            <div class="col-xs-6">

                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <th style="width:50%">Name</th>
                                            <td><input name="name" type="text" class="form-control"
                                                       value="<?php echo $building['name']; ?>"></td>
                                        </tr>

                                        <tr>
                                            <th style="width:50%">Phone</th>
                                            <td><input name="phone" type="text" class="form-control"
                                                       value="<?php echo $building['phone']; ?>"></td>
                                        </tr>


                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="col-xs-6">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>

                                        <tr>
                                            <th style="width:50%">Company Name</th>
                                            <td><input name="company_name" type="text" class="form-control"
                                                       value="<?php echo $building['company_name']; ?>"></td>
                                        </tr>

                                        <tr>
                                            <th style="width:50%">Email</th>
                                            <td><input name="email" type="text" class="form-control"
                                                       value="<?php echo $building['email']; ?>"></td>
                                        </tr>

                                        </tbody>
                                    </table>
                                    <div class="clearfix text-right" style="padding: 10px">
                                        <input type="submit" value="Submit" class="btn">
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="block clearfix">
                    <h4>Building details <a href="javascript:void(0)" class="editBuildingDetail">Edit</a></h4>
                    <div class="info-details">
                        <div class="col-xs-6">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <?php
                                    if ($building['country']) {
                                        ?>
                                        <tr>
                                            <th style="width:50%">Country</th>
                                            <td><?php echo $building['country']; ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php
                                    if ($building['province']) {
                                        ?>
                                        <tr>
                                            <th style="width:50%">Province</th>
                                            <td><?php echo $building['province']; ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php
                                    if ($building['address_1']) {
                                        ?>
                                        <tr>
                                            <th style="width:50%">Address 1</th>
                                            <td><?php echo $building['address_1']; ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th style="width:50%">Area</th>
                                        <td><?php echo $building['area']; ?></td>
                                    </tr>
                                    <tr>
                                        <th style="width:50%">Office Area</th>
                                        <td><?php echo $building['office_area']; ?></td>
                                    </tr>


                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-xs-6">


                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>

                                    <tr>
                                        <th style="width:50%">City</th>
                                        <td><?php echo $building['city']; ?></td>
                                    </tr>

                                    <tr>
                                        <th style="width:50%">Post Code</th>
                                        <td><?php echo $building['postcode']; ?></td>
                                    </tr>

                                    <tr>
                                        <th style="width:50%">Address 2</th>
                                        <td><?php echo $building['address_2']; ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th style="width:50%">Other Area</th>
                                        <td><?php echo $building['other_area']; ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="edit-form-details clearfix" style="display: none">
                        <form id="formEditBuildingDetail" action="" method="post">
                            <input type="hidden" name="id" value="<?php echo $building['id']; ?>">
                            <div class="col-xs-6">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>

                                        <tr>
                                            <th style="width:50%">Country</th>
                                            <td><input name="country" class="form-control" type="text"
                                                       value="<?php echo $building['country']; ?>"/></td>
                                        </tr>

                                        <tr>
                                            <th style="width:50%">Province</th>
                                            <td><input name="province" class="form-control" type="text"
                                                       value="<?php echo $building['province']; ?>"/></td>
                                        </tr>

                                        <tr>
                                            <th style="width:50%">Address 1</th>
                                            <td><input name="address_1" class="form-control" type="text"
                                                       value="<?php echo $building['address_1']; ?>"/></td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <th style="width:50%">Area</th>
                                            <td><input name="area" class="form-control" type="text"
                                                       value="<?php echo $building['area']; ?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="width:50%">Office Area</th>
                                            <td><input name="office_area" class="form-control" type="text"
                                                       value="<?php echo $building['office_area']; ?>"/></td>
                                        </tr>


                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-xs-6">


                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>

                                        <tr>
                                            <th style="width:50%">City</th>
                                            <td><input name="city" class="form-control" type="text"
                                                       value="<?php echo $building['city']; ?>"/></td>
                                        </tr>

                                        <tr>
                                            <th style="width:50%">Post Code</th>
                                            <td><input name="postcode" class="form-control" type="text"
                                                       value="<?php echo $building['postcode']; ?>"/></td>
                                        </tr>

                                        <tr>
                                            <th style="width:50%">Address 2</th>
                                            <td><input name="address_2" class="form-control" type="text"
                                                       value="<?php echo $building['address_2']; ?>"/></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <th style="width:50%">Other Area</th>
                                            <td><input name="other_area" class="form-control" type="text"
                                                       value="<?php echo $building['other_area']; ?>"/></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="clearfix text-right" style="padding: 10px">
                                        <input type="submit" value="Submit" class="btn">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="financial">
                <div>
                    <div class="text-right" style="border-bottom: 1px #ccc solid;padding: 10px">
                        <a href="" data-toggle="modal" data-target="#modal-invoice">Create Invoice</a>
                    </div>
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Date</th>
                            <th>Type</th>
                            <th>Account</th>
                            <th>No</th>
                            <th>memo</th>
                            <th class="text-right">Increase</th>
                            <th class="text-right">Decrease</th>
                            <th  class="text-right">Balance</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        $balance = 0;
                        if (isset($invoices) && is_array($invoices)) {
                            $i = 0;
                            foreach ($invoices as $item) {
                                $i++;
                                $balance += (float)$item['increase'];
                                $balance -= (float)$item['decrease'];
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo date('m-d-Y', strtotime($item['date'])) ?></td>
                                    <td><?php echo Common::payment_type($item['type']) ?></td>
                                    <td></td>
                                    <td><?php echo $item['no'] ?></td>
                                    <td><?php echo $item['memo'] ?></td>
                                    <td class="text-right"><?php echo ($item['increase']) ? '$' . number_format($item['increase']) : ' --- ' ?></td>
                                    <td class="text-right"><?php echo ($item['decrease']) ? '$' . number_format($item['decrease']) : ' --- ' ?></td>

                                    <td class="text-right"><?php echo '$' . number_format($balance) ?></td>

                                </tr>
                            <?php }
                        } ?>
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="tab-pane" id="task">
                <div>updating...</div>
            </div>
            <div class="tab-pane" id="event_history">
                <div>updating...</div>
            </div>
            <div class="tab-pane" id="vendor">
                <div>updating...</div>
            </div>
            <div class="tab-pane" id="tenants">
                <div class="tab-pane active" id="tenants">

                    <div class="text-right" style="border-bottom: 1px #ccc solid;padding: 10px">
                        <a href="" data-toggle="modal" data-target="#modal-default">Add Tenant</a>
                    </div>
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th>#</th>

                                <th>Tenant</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>date</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (isset($buildingTenant) && is_array($buildingTenant)) {
                                $i = 0;
                                foreach ($buildingTenant as $item) {
                                    $i++;
                                    ?>
                                    <tr>
                                        <td><?php echo $i ?></td>
                                        <td><?php echo $item['tenant_name']; ?></td>
                                        <td><?php echo $item['tenant_phone']; ?></td>
                                        <td><?php echo $item['tenant_email']; ?></td>
                                        <td><?php echo date('Y-m-d', strtotime($item['created_at'])) ?></td>
                                    </tr>
                                <?php }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="4">Empty</td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>

                        <!-- /.box-body -->

                        <!-- /.box-footer -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /.tab-content -->
    </div>

    <!-- /.nav-tabs-custom -->
</div>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Tenant</h4>
            </div>
            <div class="modal-body">
                <?php $this->load->view('building_popup_add_tenant', array('id' => $building['id'])); ?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<script>
    $('.editBuildingContact').click(function () {
        $('.info').toggle('slow');
        $('.edit-form-contact').toggle('slow');
    });
    $('.editBuildingDetail').click(function () {
        $('.info-details').toggle('slow');
        $('.edit-form-details').toggle('slow');
    });

    $(document).ready(function () {
        $('form#formEditBuildingDetail, form#formEditBuildingInfo').submit(function (event) {
            var formData = $(this).serialize();
            // process the form
            $.ajax({
                type: 'POST',
                url: base_url + '/admin/building/ajax_submit_building',
                data: formData,
                dataType: 'json',
                encode: true
            })
                .success(function (data) {
                    window.location.reload();
                });
            event.preventDefault();
        });

    });
</script>

<style>
    .edit-form-contact, .edit-form-details {
        border-left: 5px #3c8dbc80 solid;
        background: #f8f8f8;
    }

    .edit-form table {
        margin-bottom: 0;
    }

    .block h4 {
        font-size: 16px;
        font-weight: bold;
    }

    .block h4 a {
        padding-left: 10px;
        font-size: 12px;
        font-weight: normal;
    }

    .block .table tr th {
        font-weight: normal;
        color: #868686;
    }

    .nav-tabs-custom > .nav-tabs > li:first-of-type.active > a {
        font-weight: bold;
    }
</style>