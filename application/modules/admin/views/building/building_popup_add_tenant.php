<form role="form" id="create" action="" method="post" autocomplete="off">
    <div class="box-body">
        <input type="hidden" value="<?php echo isset($id)?$id:0?>" name="building_id">
        <div class="col-sm-6">

            <div class="form-group">
                <label for="name">Name <span class="text-red">*</span></label>
                <input type="text" class="form-control" name="name" id="name"
                       placeholder="Name" required autocomplete="off">
            </div>
            <div class="form-group">
                <label for="phone">Phone</label>
                <input type="text" class="form-control" id="phone" name="phone"
                       placeholder="Phone">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email"name="email"
                       placeholder="Email">
            </div>
            <div class="form-group">
                <label for="area">Space Type</label>
                <input type="text" class="form-control" id="space_type"name="space_type"
                       placeholder="Space Type">
            </div>
            <div class="form-group">
                <label for="area">Gross Area</label>
                <input type="number" class="form-control" id="gross_area"name="gross_area"
                       placeholder="Gross Area">
            </div>
            <div class="form-group">
                <label for="office_area">Lease Area</label>
                <input type="text" class="form-control" id="lease_area"name="lease_area"
                       placeholder="Office Area">
            </div>
            <div class="form-group">
                <label for="warehouse_area">Certified Area</label>
                <input type="text" class="form-control" id="certified_area" name="certified_area"
                       placeholder="Certified Area">
            </div>
            <div class="form-group">
                <label for="other_area">Other Area</label>
                <input type="text" class="form-control" id="other_area" name="other_area"
                       placeholder="Other Area">
            </div>
            <div class="form-group">
                <label for="other_area">Lease Date Start</label>
                <input type="text" class="form-control datepicker" id="lease_date_start" name="lease_date_start"
                       placeholder="Other Area">
            </div>

        </div>

        <div class="col-sm-6">
            <div class="form-group">
                <label for="company_name">Company Name<span class="text-red">*</span></label>
                <input type="text" class="form-control" name="company_name" id="company_name"
                       placeholder="Company Name" required>
            </div>
            <div class="form-group">
                <label for="attention">Attention<span class="text-red">*</span></label>
                <input type="text" class="form-control" name="attention" id="attention"
                       placeholder="Attention" required>
            </div>
            <div class="form-group clearfix">
                <label for="country">Country</label>
                <input type="text" class="form-control col-xs-6" id="country" name="country" placeholder="Country">
            </div>
            <div class="form-group clearfix">
                <label for="city">City</label>
                <input type="text" class="form-control col-xs-6" id="city" name="city" placeholder="City">
            </div>
            <div class="form-group clearfix">
                <label for="province">Province</label>
                <input type="text" class="form-control col-xs-6" id="province" name="province" placeholder="Province">
            </div>
            <div class="form-group clearfix">
                <label for="post_code">Post Code</label>
                <input type="text" class="form-control col-xs-6" id="post_code" name="post_code" placeholder="Post Code">
            </div>
            <div class="form-group clearfix">
                <label for="exampleInputEmail1">Address 1</label>
                <input type="text" class="form-control col-xs-6" id="address_line_1" name="address_line_1" placeholder="Address line 1">
            </div>
            <div class="form-group clearfix">
                <label for="exampleInputEmail1">Address 2</label>
                <input type="text" class="form-control col-xs-6" id="address_line_2" name="address_line_2" placeholder="Address line 2">
            </div>
            <div class="form-group">
                <label for="other_area">Lease Date End</label>
                <input type="text" class="form-control datepicker" id="lease_date_ends" name="lease_date_ends"
                       placeholder="Other Area">
            </div>
        </div>


        <!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
</form>
<style>
    .modal-dialog {
        width: 70%;
        margin: 5% auto auto;
    }
    .modal-header{
        border-top: 3px #3c8dbc solid;
    }
    .modal-title {
        font-size: 16px;
        font-weight: bold;
        text-transform: uppercase;
    }
</style>

<script>
    $(document).ready(function () {
        $('form#create').submit(function (event) {
            var formData = $(this).serialize();
            // process the form
            $.ajax({
                type: 'POST',
                url: base_url + '/admin/tenant/ajax_submit_tenant',
                data: formData,
                dataType: 'json',
                encode: true
            })
                .success(function (data) {
                         window.location.href= (base_url + 'admin/building');
                });
            event.preventDefault();
        });

    });


    $(document).ready(function () {
        $('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
    });

</script>