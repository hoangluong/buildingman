<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
    button.btn-primary {
        width: 80px;
    }

    .paddingtop18 {
        padding-top: 17px !important;
    }

    th {
        text-align: center;
    }

    .col-sm-6 input[type="radio"] {
        margin: 5px;

    }
</style>
<div>
    <section class="content-header">
        <h1><?php echo $page_title; ?></h1>
    </section>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="clearfix">
            <div class="pull-right">
                <a href="<?php echo base_url('admin/building/create') ?>" class="btn btn-info">Add Building</a>
            </div>
        </div>
    </div>
    <br>
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php echo '<b>' . $max . '</b> ' . $title ?> matchs
            </div>

            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Location</th>
                            <th>Rental Owners</th>
                            <th>Type</th>
                            <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        //      print_r($buildings);
                        if (isset($buildings) && is_array($buildings)) {
                            foreach ($buildings as $item) {
                                $item = (array)$item;
                                $i++;
                                ?>
                                <tr>
                                    <td class="table_check text-center paddingtop18"><?php echo $i; ?></td>
                                    <td class="paddingtop18"><a
                                                href="<?php echo base_url('admin/building/detail/' . $item['id']); ?>"><?php echo $item['address_street'] ?></a>
                                    </td>
                                    <td><?php echo $item['address_street_2']; ?></td>
                                    <td><?php echo $item['owner_name']; ?></td>
                                    <td><?php echo ((int)$item['type'] > 0) ? Common::building_type($item['type']) : ''; ?></td>
                                    <td  class="text-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default">...</button>
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Units</a></li>
                                                <li><a href="#">Financials</a></li>

                                                <li class="divider"></li>
                                                <li><a href="#">Building summary</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"
                                 style="float: right;">
                                <ul class="pagination" style="margin: 0px;">

                                    <?php
                                    for ($i = 1; $i <= $total_page; $i++) {

                                        ?>

                                        <li class="paginate_button <?php echo ($page == $i) ? 'active' : ''; ?>"
                                            aria-controls="dataTables-example"
                                            tabindex="0">
                                            <a href="<?php echo site_url('admin/building/index/' . $i) ?>"><?php echo $i ?></a>
                                        </li>
                                        <?php


                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>


                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->

        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<style>
    .table-responsive {
        overflow-x: inherit;
    }
</style>