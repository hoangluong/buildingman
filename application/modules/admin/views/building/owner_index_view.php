<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
    button.btn-primary {
        width: 80px;
    }

    .paddingtop18 {
        padding-top: 17px !important;
    }

    th {
        text-align: center;
    }

    .col-sm-6 input[type="radio"] {
        margin: 5px;

    }
</style>

<div class="row">
    <div class="col-lg-12">
        <div class="clearfix">
            <div class="pull-right">
                <a href="<?php echo base_url('admin/building/create') ?>" class="btn btn-info">Add Building</a>
            </div>
        </div>
    </div>
    <br>

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php echo '<b>' . $max . '</b> ' . $title ?>
            </div>

            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Address</th>
                            <th>Buildings</th>
                            <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        //   print_r($buildings);
                        if (isset($owners) && is_array($owners)) {
                            foreach ($owners as $item) {
                                $item = (array)$item;
                                $i++;
                                ?>
                                <tr>
                                    <td class="table_check text-center paddingtop18"><?php echo $i; ?></td>
                                    <td class="paddingtop18"><a
                                            href="<?php echo base_url('admin/building/detail/' . $item['id']); ?>"><?php echo $item['name'] ?></a>
                                    </td>
                                    <td><?php echo $item['description']; ?></td>
                                    <td><?php echo $item['address']; ?><br><?php echo $item['phone']; ?><br><?php echo $item['email']; ?></td>
                                    <td class="text-center"><a href=""><?php echo $item['building_number'];?></a></td>
                                    <td><a href="<?php echo base_url('admin/building/edit/' . $item['id']); ?>"><i
                                                class="glyphicon glyphicon-edit"></i></a></td>

                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"
                                 style="float: right;">
                                <ul class="pagination" style="margin: 0px;">

                                    <?php
                                    for ($i = 1; $i <= $total_page; $i++) {

                                        ?>

                                        <li class="paginate_button <?php echo ($page == $i) ? 'active' : ''; ?>"
                                            aria-controls="dataTables-example"
                                            tabindex="0">
                                            <a href="<?php echo site_url('admin/building/index/' . $i) ?>"><?php echo $i ?></a>
                                        </li>
                                        <?php


                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>


                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->

        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<style>
    .table-responsive {
        overflow-x: inherit;
    }
</style>