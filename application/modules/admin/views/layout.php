<?php $this->load->view('header') ?>
<?php $this->load->view('sidebar'); ?>
    <div class="content-wrapper" style="min-height: 946px;margin-left: 0;">
        <div class="main-menu">
            <div class="container">
                <ul>
                    <li><a href="">Retals</a>
                        <ul>
                            <li><a href="<?php echo base_url('admin/building');?>">Buildings</a></li>
                            <li><a href="<?php echo base_url('admin/rentroll');?>">Rent Roll</a></li>
                            <li><a href="<?php echo base_url('admin/tenant');?>">Tenants</a></li>
                            <li><a href="">Rental Owner</a></li>
                        </ul></li>
                    <li><a href="">Leasing</a></li>

                </ul>
            </div>
        </div>
        <div class="container">

            <!-- Main content -->
            <section class="content clearfix">
                <?php $this->load->view($template); ?>
            </section><!-- /.content -->
        </div>
    </div>

<?php $this->load->view('footer') ?>