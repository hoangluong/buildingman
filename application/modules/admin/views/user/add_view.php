<link rel="stylesheet" href="<?php echo STATIC_DOMAIN ?>skins/admin/jquery-ui-1.8.22.custom.css"/>
<script type="text/javascript">
    $(document).ready(function () {
        $('#dateadd').datepicker({dateFormat: 'yy-mm-dd'});
    });

</script>
<div style="background:white;">
    <div
        style="background:white;width:600px; text-align:left; padding:10px 10px 10px 103px;color: red;"><?php echo validation_errors(); ?>
    </div>
    <form action="" method="post">
        <label for="contact"><b>Contact:</b>
            <input type="text" id="contact" name="contact" style="border: 1px solid #7AB7E8;width: 200px;"/><br/>
        </label><BR/><BR/>
        <label for="email"><b>*Email:</b>
            <input type="text" id="email" name="email_address" style="border: 1px solid #7AB7E8;width: 200px;"/><br/>
        </label><BR/><BR/>
        <label for="user"><b>*User Name:</b>
            <input type="text" id="user" name="username" style="border: 1px solid #7AB7E8;width: 200px;"/><br/>
        </label><BR/><BR/>
        <label for="phone"><b>*PassWord:</b>
            <input type="password" id="password" name="password" style="border: 1px solid #7AB7E8;width: 200px;"/><br/>
        </label><BR/><BR/>
        <label for="dateadd"><b>Date:</b>
            <input type="text" id="dateadd" name="dateadd" style="border: 1px solid #7AB7E8;width: 200px;"/><br/>
        </label><BR/><BR/>
        <label for="group" style="float: left;"><b style="float: left;">Position</b>
            <select name="is_position" style="border: 1px solid #7AB7E8;float: left;margin-left: 30px;width: 145px;">
                <?php foreach ($listPositions as $position) {
                    ; ?>
                    <option value="<?php echo $position['id']; ?>"><?php echo $position['name']; ?></option>
                <?php }; ?>
            </select>
        </label><BR/><BR/><BR/>
        <label for="group"><b>Group</b>
            <select name="is_admin" style="border: 1px solid #7AB7E8;margin-left: 30px;width: 145px;">
                <?php foreach ($listDepartments as $department) {
                    ; ?>
                    <option value="<?php echo $department['id']; ?>"><?php echo $department['name']; ?></option>
                <?php }; ?>
            </select>
        </label><BR/><BR/>
        <label for="group"><b>Permission</b>
            <select name="is_permit" style="border: 1px solid #7AB7E8;margin-left: 30px; width: 145px;">
                <?php foreach ($listPermission as $permit) {
                    ; ?>
                    <option value="<?php echo $permit['id']; ?>"><?php echo $permit['name']; ?></option>
                <?php }; ?>
            </select>
        </label>

        <div><input type="submit" name="submit" value="Add user"
                    style="background: none repeat scroll 0 0 #4780AE;border: 1px solid #7AB7E8;  color: #FFFFFF; display: block; margin: 21px 15px -10px 138px; padding: 4px;"></input>
        </div>
    </form>
</div>
	