<div id="content_main" class="clearfix">
    <div id="main_panel_container" class="left">
    <div id="dashboard">
        <h2 class="ico_mug">Order Leader - Vị trí leader</h2>
        <div class="clearfix">           			
			<ul id="sortable">
				<?php foreach($sorts as $sort):?>
				<?php foreach($leaders as $leader):?>					
						<?php if($leader['u_id'] == $sort):?>					
							<li class="ui-state-default" id="<?php echo $leader['u_id']?>"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><?php echo $leader['u_name']?></li>
						<?php endif;?>	
					<?php endforeach;?>
				<?php endforeach;?>
				<?php foreach($leaders as $leader):?>
					<?php if(in_array($leader['u_id'], $sorts) == false):?>
							<li class="ui-state-default" id="<?php echo $leader['u_id']?>"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><?php echo $leader['u_name']?></li>
					<?php endif;?>
				<?php endforeach;?>				
			</ul>         
        </div>
        <div>
        	<button id="add_sort"><span>Cập nhật</span></button>
        	<p id="update_result"></p>
        </div>
    </div><!-- end #dashboard -->		
    </div>
    
    <script type="text/javascript">
    $(document).ready(function(){		
    	$( "#sortable" ).sortable();
		$( "#sortable" ).disableSelection();

		$(".ui-state-default").mousedown(function(){
			$("#update_result").hide();
		});
		
		$("#add_sort").click(function(){
			var order_sort = "";
			$("#sortable li").each(function(){				
				order_sort += $(this).attr("id")+",";				
			});
			ajaxData = {};
			ajaxData['order'] = order_sort;					
			$.ajax({
				url: "/admin/user/order",
				type: 'post',							
				data: ajaxData,
				success:function(response){										
					if(response == 1){
						$("#update_result").show();
						$("#update_result").html("Cập nhật thành công!");
						$("#update_result").css("color","green");						
					}
					else{
						$("#update_result").show();
						$("#update_result").html("Cập nhật không thành công, vui lòng thử lại!");
						$("#update_result").css("color","red");
					}
				}
			});				
		});
	});
    </script>
    