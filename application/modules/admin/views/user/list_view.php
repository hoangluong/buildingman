<script type="text/javascript">
    $(document).ready(function() {
        $(".selectAll").click(function() {
            if($(this).is(':checked')) {
                $(".selectPost").attr('checked','checked');
            } else {
                $(".selectPost").removeAttr('checked');
            }
        });
        
        $(".selectPost").click(function() {
            if($('.selectPost:checked').length != $('.selectPost').length)
                $(".selectAll").removeAttr('checked');
            else 
                $(".selectAll").attr('checked','checked');
        });
    
        $(".SelectAll").click(function(){
            $(".selectPost").attr('checked','checked');
            $(".selectAll").attr('checked','checked');
            return false;
        });
        // href="<?php echo base_url();?>admin/user/delete?id=<?php echo $row["u_id"];?>"
        $(".actionPost").click(function() {
          
            var answer = confirm('Bạn có chắc chắn muốn xóa không ?');
             if(answer){
             //   alert("you die");
             location.href = "<?php echo base_url();?>admin/user/delete?id="+$(this).attr('lang');
             }
        });
        
        $(".SelectNone").click(function() {
            $(".selectPost").removeAttr('checked');
            $(".selectAll").removeAttr('checked');
            return false;
        });
        
        $("#acceptOption").click(function() {
            if($(".selectPost:checked").length == 0) {
                alert('Bạn cần chọn ít nhất 1 tin bài để sử dụng chức năng này !');
                return false;
            }
            
            if($("#optionPost").val() == '') {
                alert('Bạn cần lựa chọn hành động cho các tin bài này !');
                return false;
            }
            
            var selectPost = $('.selectPost:checked').map(function() {
                return $(this).val();
            }).get();
            var type = $("#optionPost").val();
            $.ajax({
                url : base_url + 'admin/adinfo/change',
                type : 'POST',
                dataType : 'json',
                data : 'selectPost=' +selectPost+ '&type=' + type,
                success : function(data) {
                    if(data.error == '') {
                        $(".selectPost").removeAttr('checked');
                        $(".selectAll").removeAttr('checked');
                        $("#optionPost").val('');
                        location.reload();
                    } else {
                        alert(data.error);
                    }
                }
            });
        });
    });
</script>
<style type="text/css">
.active
{
    background: url("../../../images/default/bg-paghover.jpg") no-repeat scroll 0 0 transparent !important;
    color: #FFFFFF;
    display: block;
    float: left;
    text-align: center;
    width: 20px;  
    margin: 5px !important;
    padding:2px 0 0 !important; 
}
.pagination a
{
    float: left;
    border: 1px solid #D9D9D9;
    height: 18px;
    margin: 5px;
    padding: 2px 0 0;
    text-align: center;
    text-decoration: none;
    width: 18px;
}
input.hhd_search
{
    color: #666666 !important;
}
</style>
    <div style="background:white; padding: 10px;">     
        <div style="background:white;font-weight: bold;color:red;">
            <div style="float: left;width: 50%;">
                <a href="<?php echo site_url();?>admin/user/add" ><img border="0"  src="<?php echo site_url()?>/images/admin/add_user.gif" />THÊM USER</a>
            </div>
            <div style="width: 50%;">
                <form action="<?php echo site_url(); ?>admin/search/result" method="get" > 
                    <input type="text" maxlength="100" name="keyword" value=" Search user" size="30" class="hhd_search" onfocus="this.select()" onclick="this.value='';" />
    	        </form>
            </div>
            
        </div>                      
       
            <div class="content">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr height="30">
                        <th width="10%" align="left">user name</th>
                        <th width="10%" align="left">Email</th>
                        <th width="13%" align="left">Contact</th>
                        <th width="13%" align="left">Delete</th>
                        <th width="13%" align="left">Edit</th>
                    </tr>         
                    <?php
                    $i = 0;
                    foreach($lstuser as $row){ 
						$bgcolor = ($i%2==0) ? "#f6f6f6" : "#ffffff";
						$i++
						
					?>               
                    <tr height="30"  style="background:<?php echo $bgcolor;?>">
                        <td ><div style="float: left; padding-right: 5px;"><a href="#"><img border="0" src="<?php echo site_url()?>/images/admin/triangle-l.gif" /></a></div>
                         <div><a href=""><?php echo $row["u_acc"];?></a></div></td>
                         <td><?php  echo $row["u_email"]?></td>                         
                         <td><?php  echo $row["u_name"]?></td>
                         <td  style="padding-right: 10px;">
                            <a href="#" lang="<?php echo $row['u_id']?>" class="actionPost"><img border="0"  src="<?php echo site_url()?>/images/admin/delete_user.gif" />Delete</a>
                         </td>
                         <td  style="padding-right: 10px;"><a href="<?php echo base_url();?>admin/user/edit/<?php echo $row["u_id"];?>">Edit</a></td>
                    </tr>        
                    <?php }?>            
                </table>
            </div>
            <div class="pagination" style="height: auto; max-height: 99%; overflow: auto; padding-bottom: 10px;padding-top:10px;">
                   <?php echo $link;?>
        	
            </div>
    </div>    



