<?php $asset = base_url('public/admin/');?><html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="icon" href="<?php echo base_url('favicon.ico')?>" />
    <link rel="shortcut icon" href="<?php echo base_url('favicon.ico')?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="<?php echo $asset.'bootstrap/css/bootstrap.min.css';?>" rel="stylesheet">
    <link href="<?php echo $asset.'admin/dist/css/AdminLTE.min.css';?>" rel="stylesheet">
    <link href="<?php echo $asset.'css/login.css';?>" rel="stylesheet">
    <script src="<?php echo $asset.'js/jquery-1.12.4.min.js';?>"></script>
</head>
<body>