<?php $this->load->view('user/header');?>
<div class="container" style="margin: 0;position: absolute;top: 50%;left: 50%;margin-right: -50%;transform: translate(-50%, -50%);padding: 10px">
    <img src="<?php echo site_url('/public/images/logo.png')?>" style="display: block; margin-right: auto; margin-left: auto;margin-bottom: 25px">
    <div class="container">
        <div class="row">
            <form class="form-horizontal form-auth" name="SignInform" action="" method="post">
                <input type="hidden" name="frmToken" id="frmToken" value="<?php echo $frmToken ?>"/>
                <h1 class="page-title">Forget password</h1>
                <?php if (isset($message)) { ?>
                    <!-- alert alert-info alert-dismissible -->
                    <div style="display: block;"
                         class="alert <?php echo $message['success'] ? 'alert-info ' : 'alert-danger '; ?>alert-dismissible"
                         id="message">
                        <button type="button" id="btn-close-msg" class="close" data-dismiss="alert"
                                aria-hidden="true">×
                        </button>
                        <p id="message-content"><?php echo $message['msg']; ?></p>
                    </div>
                <?php } ?>
                <div class="form-group">
                    <label for="email" class="control-label col-xs-4">Email</label>
                    <div class="col-xs-8">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Enter your email">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-offset-4 col-xs-8">
                        <button type="submit" class="btn btn-default">Send</button>
                        <a href="<?php echo base_url('login')?>" class="btn btn-link"><?php echo htmlentities('<<');?> Back</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $this->load->view('user/footer');?>