<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Dinh Thuan <thuannguyendinh@admicro.vn>
 * @copyright 2014
 */
class General_model extends CI_Model
{

    
     public function get_match($tbl)
    {
        $this->db->select('*');
        $this->db->from($tbl);
        $rs = $this->db->get();
        if($rs->num_rows == 0) {
            $rs->free_result();
            show_404();
        }
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }
    public function getBetting_byMatch($id,$tbl,$field,$field_2='',$id_field_2='',$field_3='',$id_field_3='',$field_4='',$id_field_4='')
    {
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->where($field,$id);
        if($field_2 != '' && $id_field_2 != '') $this->db->where($field_2,$id_field_2);
        if($field_3 != '' && $id_field_3 != '') $this->db->where($field_3,$id_field_3); 
        if($field_4 != '' && $id_field_4 != '') $this->db->where($field_4,$id_field_4); 
        $rs = $this->db->get();
        
        $return = $rs->result_array();
        $rs->free_result();
        
        return $return;
    }
    
   public function add($data,$tbl)
   {
        $data = $this->db->escape($data);
        return ($this->db->insert($tbl,$data)) ? $this->db->insert_id() : false;
   }
   
   function update($id,$data,$tbl,$field){
        $data = $this->db->escape($data);
        $id = intval($id);
        $this->db->where($field,$id);
        if($this->db->update($tbl,$data))
            return true;
        else
            return false;
    }
    
    /* Get tổng số bản ghi*/
   public function getRecordByKey($key = NULL, $tbl, $field = NULL)
      {
            $this->db->from($tbl);
            if ($key != NULL && $field != NULL)  $this->db->where($field,$key);
            return  $this->db->count_all_results();
      }
      
    /*Get tất cả các bản ghi*/
   public function get_listRecord($limit,$off, $tbl,$field_sort, $type = NULL, $id = NULL, $type_1 = '', $id_1 = '')
      {
       
    	    $data = array();		    
    	  	$this->db->select('*');
    	  	$this->db->from($tbl);    
            if($type != NULL && $id != NULL) $this->db->where($type,$id); 
            else{
                if ($type != NULL ) $this->db->where('type',$type);
            }    
            if($type_1 != NULL && $id_1 != NULL) $this->db->where($type_1,$id_1); 
            $this->db->limit($limit,$off);
            $this->db->order_by($field_sort,"asc"); 
    	  	$query = $this->db->get();
    	  	$data= $query->result_array();
    	  	return $data;
            
      }
   public function get_record_like($limit,$off, $tbl,$field_sort, $type = NULL, $id = NULL, $type_1 = '', $id_1 = '')
      {
            $id = intval($id);
            $id_1 = intval($id_1);
       
    	    $data = array();		    
    	  	$this->db->select('*');
    	  	$this->db->from($tbl);    
            if($type != NULL && $id != NULL) $this->db->like($type,$id); 
            else{
                if ($type != NULL ) $this->db->where('type',$type);
            }    
            if($type_1 != NULL && $id_1 != NULL) $this->db->where($type_1,$id_1); 
            $this->db->limit($limit,$off);
            $this->db->order_by($field_sort,"desc"); 
    	  	$query = $this->db->get();
    	  	$data= $query->result_array();
    	  	return $data;
            
      }
   public function getInfo_byID($id,$tbl,$field)
    {
        $id = intval($id);
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->where($field, $id);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }
   
    
    function num_rows_search($name, $id_cate = '', $tbl, $type, $type1 = ''){
        $name = $this->db->escape($name);
        $id_cate = intval($id_cate);
        $this->db->select('*');
        $this->db->from($tbl);       		
        $this->db->like($type, $name); 
        if($id_cate != '' || $id_cate != NULL || $id_cate != '-1') $this->db->like($type1, $id_cate);                     		
		$rs =  $this->db->get();        
        $return =  $rs->num_rows; 
       	return $return; 	  
  }
  
  
    
    public function deletePost($post, $tbl)
    {
        $post = intval($post);
        if(is_array($post)) {
            $this->db->where_in('id', $post);
        } else {
            $this->db->where_in('id', $post);
        }
        if($this->db->delete($tbl))
                return true;
        return false;
    }
    
    public function changePost($id, $status = NULL, $tbl)
    {
        $array = array();
        $id = intval($id);  
        
        if ($status !== NULL)
            $array['status'] = $status;
        
        if(is_array($id))
            $this->db->where_in('id', $id);
        else
            $this->db->where('id', $id);
        if($this->db->update($tbl, $array))
            return true;
        else
            return false;
    }
    
    public function change_form($id, $status = NULL, $tbl)
    {
        
        $array = array();
        
        if ($status !== NULL)
            $array['form'] = $status;
        
        $this->db->where_in('id', $id);
        if($this->db->update($tbl, $array))
            return true;
        else
            return false;
    }
    
     function searchPaging($filter = null,$name, $off,$perpage, $tbl, $type, $sort){	
		
        $this->db->select('*');
        $this->db->from($tbl);
        if($filter){
			$this->db->where($filter);
		}		
        $this->db->like($type, $name); 
        $this->db->order_by($sort,"desc");
        $this->db->limit($perpage, $off);  
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
        
	}
      
    
}

?>
