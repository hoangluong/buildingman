<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_admin_user extends CI_Model
{

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function login($u_acc, $u_pwd)
    {
        $name = $this->db->escape($u_acc);
        $name = $this->db->escape($u_pwd);
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('u_acc' , $u_acc );
        $this->db->where('u_pwd', MD5($u_pwd) );
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->result();
    }

  function num_rows(){   	   
       $this->db->from('user');
       return  $this->db->count_all_results();
  	  
   }
    public function getEmail()
    {
        $this->db->select('*');
        $this->db->from('user');
        $result= $this->db->get();
        if($result->num_rows == 0)
            return false;
        $return = $result->result_array();
        $result->free_result();
        return $return;
    }
	
	
    public function getInfoUser_byID($id)
    {
        $id = intval($id);
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('u_id', $id);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }
}

