<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @author Thuan Nguyen Dinh<thuannguyendinh@admicro.vn>
 * @copyright 2014
 */
class Search_model extends CI_Model {
	function searchByLastName($name){
        $name = $this->db->escape($name);
		$this->db->select('*');
        $this->db->from('user');
        	       
		$this->db->like('u_acc', $name);
        $this->db->or_like('title',$name);
        $this->db->where('type','1');
		return $this->db->get(); 
	}
  function num_rows($name){
        $name = $this->db->escape($name);
        $this->db->select('*');
        $this->db->from('user');                		
        $this->db->like('u_acc', $name);    
        $this->db->or_like('u_email', $name); 
        $this->db->or_like('u_name', $name);                       		
		$rs =  $this->db->get();        
        $return=  $rs->num_rows;         
       	return $return;
        //print_r($return);die; 	  
  }
  
function searchByLastNamePaging($name, $page,$perpage){	
        $name = $this->db->escape($name);
		$this->db->select('*');
        $this->db->from('user');                		
        $this->db->like('u_acc', $name);    
        $this->db->or_like('u_email', $name); 
        $this->db->or_like('u_name', $name);    
        $this->db->limit($perpage, $page);                       		
		$rs =  $this->db->get();
		$return = $rs->result_array();  
       	return $return;
	}
}

