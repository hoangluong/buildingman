<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends Base_model {

    protected $table = 'category';
    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->created_on = 'time';
        $this->modified_on = '';
    }
    function get_cond($cond){
        if(isset($cond['s']) && $cond['s']){
            $this->db->group_start();
            $this->db->or_like('name', $cond['s']);
            $this->db->group_end();
            unset($cond['s']);
        }
        return $cond;
    }
    function find($cond, $page, $per_page, $order=array()){
        $cond = $this->get_cond($cond);
        return parent::find($cond, $page, $per_page, $order);
    }
    function total($cond){
        $cond = $this->get_cond($cond);
        return parent::total($cond);
    }
}