<?php

class Tenant_model extends Base_model
{

    protected $table = 'tenants';

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_cond($cond){
        if(isset($cond['s']) && $cond['s']){
            $this->db->group_start();
            $this->db->or_like('name', $cond['s']);
            $this->db->group_end();
            unset($cond['s']);
        }
        return $cond;
    }
    function find($cond, $page, $per_page, $order=array()){
        $cond = $this->get_cond($cond);
        return parent::find($cond, $page, $per_page, $order);
    }

    function add($data){
        $this->db->insert($this->table,$data);
        return $this->db->insert_id();
    }
    function add_building_tenant($data){
        $this->db->insert('building_tenants',$data);
        return $this->db->insert_id();
    }
}