<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin_Controller extends Vcc_Controller{
    protected $per_page = 20;
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('admin_id')){
            $this->session->set_userdata('current_url', uri_string());
            redirect('admin/login');
        }
    }
    public function del($id){
        $confirm = $this->input->get('confirm', true);
        if(!$confirm){
            $data = array(
                'redirect' => base_url(''.$this->router->fetch_class()),
                'del' => base_url(''.$this->router->fetch_class().'/del/'.$id.'?confirm=1')
            );
            echo '<script>'.
                'var r = confirm("Bạn có chắc chắn muốn xóa dữ liệu này.");'.
                'if(r==true){'.
                'window.location.href = "'.$data['del'].'";'.
                '}else{'.
                'window.location.href = "'.$data['redirect'].'";}</script>';
            exit();
        }
    }
}