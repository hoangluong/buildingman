<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Front_Controller extends Vcc_Controller{
    public function __construct()
    {
        parent::__construct();
    }
    protected function default_data(){
        $temp = array();
        /*
        $this->load->model('setting_model');
        $config = $this->setting_model->find_all_by('config_key', array('base_config', 'social_config'));
        if($config){
            foreach($config as $conf){
                $data = json_decode($conf->config_value);
                if($data){
                    foreach($data as $key=>$val){
                        $temp[$key] = $val;
                    }
                }
            }
        }*/
        return $temp;
    }
}