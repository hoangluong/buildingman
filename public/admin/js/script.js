(function(){
    this.slugEvent = function($title, $slug){
        $title.change(function(){
            slug( $title.val(), $slug )
        });
        $title.click(function(){
            if($slug.val().trim()===''){
                slug( $title.val(), $slug )
            }
        });
        $title.blur(function(){
            if($slug.val().trim()===''){
                slug( $title.val(), $slug );
            }
        });
    };
    this.slug = function(str, $slug){
        if(str.length===0) return false;
        $.ajax({
            url: base_url + 'admin/slug',
            data: {'title':str},
            type: 'post',
            dataType : "html",
            beforeSend: function(){
                $slug.attr('readonly', 'true');
            },
            success: function(result){
                console.log(result);
                $slug.val(result);
                $slug.removeAttr('readonly');
            },
            error: function(err){
                console.log(err);
                $slug.removeAttr('readonly');
            }
        });
        return true;
    };
    var initList = function(){
        /*var $delete = $('.delete');
        if($delete.length>0){
            $delete.click(function(e){
                e.preventDefault();
                console.log(e);
                var r = confirm("Bạn có chắc chắn muốn xóa dữ liệu này.");
                if(r==true){
                    window.location.href = $(this).attr('href');
                }
                return false;
            });
        }*/
    };
    var editorInit = function(){
        var $editor = $('.editor');
        if($editor.length===0) return false;
        var settings = VccFinder.getConfigMedia();
        tinymce.init(settings);
        return true;
    };
    var review_thumbnail = function(){
        var $thumbnail = $('.thumbnail');
        if($thumbnail.length==0) return false;
        $thumbnail.change(function(){
            var files = this.files;
            var $wrap_thumbnail = $(this).parent(),
                $wrap_review = $wrap_thumbnail.find('.thumbnail-review'),
                $review_thumbnail = $wrap_thumbnail.find('img');
            if(files && files[0]){
                console.log(files[0]);
                var url = URL.createObjectURL(event.target.files[0]);
                $review_thumbnail.attr('src', url).show();
                $wrap_review.show();
            }
        });
    };
    var initForm = function(){
        var $btnUploadThumbnail = $('.upload');
        if($btnUploadThumbnail.length>0){
            $btnUploadThumbnail.click(function (e) {
                e.preventDefault();
                VccFinder.openFrameUpload({
                    output : 'thumbnail',
                    mime : 'image',
                    multiple : false
                });
                return false;
            });
        }
        var $inputs = $('input[type="checkbox"].minimal, input[type="radio"].minimal');
        if($inputs.length>0){
            $inputs.iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue',
                cursor: true
            });
        }   
        var $radio = $('input[type="checkbox"].flat-red, input[type="radio"].flat-red');
        if($radio.length>0){
            $radio.iCheck({
              checkboxClass: 'icheckbox_flat-green',
              radioClass: 'iradio_flat-green'
            });
        }
        var $select = $('select.select2');
        if($select.length>0){
            $select.select2();
        }
        review_thumbnail();
        editorInit();
    };
    var formEvent = function(){
        var $name = $('#name'),
            $slug = $('#slug');
        if($name.length>0 && $slug.length>0){
            slugEvent($name, $slug);
        }
    };
    $(document).ready(function(){
        initList();
        initForm();
        formEvent();
    });
}());