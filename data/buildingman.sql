/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100126
 Source Host           : localhost:3306
 Source Schema         : buildingman

 Target Server Type    : MySQL
 Target Server Version : 100126
 File Encoding         : 65001

 Date: 10/07/2018 02:32:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for building_owner
-- ----------------------------
DROP TABLE IF EXISTS `building_owner`;
CREATE TABLE `building_owner`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `address` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `website` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `file` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of building_owner
-- ----------------------------
INSERT INTO `building_owner` VALUES (1, 'Ocean Properties LLC', 'dwe', '5555-1231', 'Boston', 'boston@gmail.com', 'http://', '');
INSERT INTO `building_owner` VALUES (2, 'pede.Cras', '', 'pede.Cras', 'pede.Cras', 'pede.Cras', 'pede.Cras', '');

-- ----------------------------
-- Table structure for building_tenants
-- ----------------------------
DROP TABLE IF EXISTS `building_tenants`;
CREATE TABLE `building_tenants`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `building_id` int(10) NOT NULL,
  `tenant_id` int(10) NOT NULL,
  `created_at` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of building_tenants
-- ----------------------------
INSERT INTO `building_tenants` VALUES (1, 3, 1, '2018-06-24 21:02:20');

-- ----------------------------
-- Table structure for buildings
-- ----------------------------
DROP TABLE IF EXISTS `buildings`;
CREATE TABLE `buildings`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `building_owner_id` int(11) NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `address_street` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `address_city` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `address_state` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `address_postcode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bank_account` int(11) NULL DEFAULT NULL,
  `bank_account_reverse` decimal(11, 0) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `address_street_2` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `address_street_3` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of buildings
-- ----------------------------
INSERT INTO `buildings` VALUES (1, NULL, 1, 1, '', '', '', '', NULL, 1, 0, NULL, NULL, NULL);
INSERT INTO `buildings` VALUES (2, NULL, 1, 1, '', 'Boston', 'Boston', '100000', NULL, 1, 200, NULL, NULL, NULL);
INSERT INTO `buildings` VALUES (3, NULL, 1, 1, '1085 12th AVE NW', 'Issaquah', 'WA ', '98027 ', NULL, 1, 1000, NULL, ' Suite D2, Issaquah, WA', ' ');

-- ----------------------------
-- Table structure for buildings_bk
-- ----------------------------
DROP TABLE IF EXISTS `buildings_bk`;
CREATE TABLE `buildings_bk`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `company_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `address_1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `address_2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `province` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `postcode` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `country` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `area` int(10) NULL DEFAULT NULL,
  `office_area` int(10) NULL DEFAULT NULL,
  `warehouse_area` int(10) NULL DEFAULT NULL,
  `other_area` int(10) NULL DEFAULT NULL,
  `user_id` int(10) NULL DEFAULT NULL,
  `created_date` datetime(0) NULL DEFAULT NULL,
  `updated_date` datetime(0) NULL DEFAULT NULL,
  `building_owner_id` int(10) NOT NULL,
  `notes` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of buildings_bk
-- ----------------------------
INSERT INTO `buildings_bk` VALUES (1, 'building', 'Company name', 'address 1', 'Address 2', 'City', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '');
INSERT INTO `buildings_bk` VALUES (2, 'Xuan Luong Hoang', 'Ông', 'Ha Noi', '', 'Ha Noi', 'United Kingdom (UK)', NULL, 'Vietnam', '983866897', 'luonghx@gmail.com', 0, 0, 0, 3, 12, '2018-06-22 02:08:32', NULL, 0, '');
INSERT INTO `buildings_bk` VALUES (3, 'Garden Row (multi-building complex)', ' Lorraine Lee', '33 Garden Row', 'Boston, MA 02210', 'Boston', 'Boston', '', 'United States', '555-555-1212', 'luonghx@gmail.com', 1, 3, 0, 2, 12, '2018-06-24 16:47:02', NULL, 0, '');
INSERT INTO `buildings_bk` VALUES (4, NULL, NULL, '33 Garden Row', 'Boston, MA 02210', 'Boston', 'Boston', '', 'United States', NULL, NULL, 1, 3, NULL, 2, NULL, NULL, NULL, 0, '');
INSERT INTO `buildings_bk` VALUES (5, 'luonghx@gmail.com', NULL, 'Ha Noi', '311 T2 - Golden An Khanh Building, Hoai Duc district', 'Ha Noi', 'United Kingdom (UK)', NULL, 'Vietnam', '983866897', 'luonghx@gmail.com', 0, 3, NULL, 311, NULL, NULL, NULL, 1, '');
INSERT INTO `buildings_bk` VALUES (6, 'Nam.nulla', 'Nam.nulla', '469-3098 Duis Ave', '', 'Sed Road', 'Duis Ave', NULL, 'USA', '5555-807 517', 'Nam.nulla@rutrumFusc', 2, 3, NULL, 3, NULL, NULL, NULL, 2, '');
INSERT INTO `buildings_bk` VALUES (7, 'Nam.nulla', NULL, '469-3098 Duis Ave', '', 'Sed Road', 'Duis Ave', NULL, 'USA', '5555-807 517', 'Nam.nulla@rutrumFusc', 2, 3, NULL, 3, NULL, NULL, NULL, 2, '');
INSERT INTO `buildings_bk` VALUES (8, 'Nam.nulla', NULL, '469-3098 Duis Ave', '', 'Sed Road', 'Duis Ave', NULL, 'USA', '5555-807 517', 'Nam.nulla@rutrumFusc', 2, 3, NULL, 3, NULL, NULL, NULL, 2, '');
INSERT INTO `buildings_bk` VALUES (9, 'Nam.nulla', NULL, '469-3098 Duis Ave', '', 'Sed Road', 'Duis Ave', NULL, 'USA', '5555-807 517', 'Nam.nulla@rutrumFusc', 2, 3, NULL, 3, NULL, NULL, NULL, 2, '');

-- ----------------------------
-- Table structure for invoices
-- ----------------------------
DROP TABLE IF EXISTS `invoices`;
CREATE TABLE `invoices`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `account` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `memo` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `increase` float NOT NULL,
  `decrease` float NOT NULL,
  `date` date NOT NULL,
  `created_at` datetime(0) NOT NULL,
  `payment_method` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `check_number` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `received_from` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `building_id` int(10) NOT NULL,
  `credit_action` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `file` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of invoices
-- ----------------------------
INSERT INTO `invoices` VALUES (1, '1', '', '1', 'Test for invoice', 1500, 0, '2018-06-25', '2018-06-25 01:50:48', '1', '114', ' ', 3, 'a', '');
INSERT INTO `invoices` VALUES (2, '1', 'Trust account', '', 'Opening balance: Security deposit\r\n\r\n', 123, 0, '0000-00-00', '0000-00-00 00:00:00', '1', '123', '', 3, 'Issue credit for payments previously deposited\r\n\r\n', '');
INSERT INTO `invoices` VALUES (3, '1', 'Trust account', '', '21312', 123123, 0, '2018-06-30', '0000-00-00 00:00:00', '1', '123', '', 5, '123123', '');
INSERT INTO `invoices` VALUES (4, '1', 'Trust account', '', '21312', 123123, 0, '2018-06-30', '0000-00-00 00:00:00', '1', '123', '', 5, '123123', '');
INSERT INTO `invoices` VALUES (5, '2', 'Trust account', '', 'g', 53, 0, '2018-06-30', '0000-00-00 00:00:00', '1', '999', '', 5, 'ergr', '');

-- ----------------------------
-- Table structure for tenants
-- ----------------------------
DROP TABLE IF EXISTS `tenants`;
CREATE TABLE `tenants`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `attention` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `address_line_1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `address_line_2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `City` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `province` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `postcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `country` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `space_type` varchar(0) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'Put option dropdown: office or warehouse or other',
  `gross_area` int(10) NULL DEFAULT NULL,
  `lease_area` int(10) NULL DEFAULT NULL,
  `certified_area` int(10) NULL DEFAULT NULL,
  `lease_date_start` date NULL DEFAULT NULL,
  `lease_date_ends` date NULL DEFAULT NULL,
  `rate` float NULL DEFAULT NULL,
  `add_rent` float NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tenants
-- ----------------------------
INSERT INTO `tenants` VALUES (1, 'Xuan Luong Hoang', 'Ông', 'Xuan Luong Hoang', 'Ha Noi', '3', 'Ha Noi', 'United Kingdom (UK)', NULL, 'Vietnam', '983866897', 'luonghx@gmail.com', '', 0, 0, 0, '0000-00-00', '0000-00-00', NULL, NULL, '2018-06-24 21:02:20', NULL);

-- ----------------------------
-- Table structure for user_serving
-- ----------------------------
DROP TABLE IF EXISTS `user_serving`;
CREATE TABLE `user_serving`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `avatar` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT 2,
  `status` int(11) NULL DEFAULT NULL,
  `last_login` datetime(0) NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `modified_on` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_serving
-- ----------------------------
INSERT INTO `user_serving` VALUES (12, NULL, NULL, NULL, NULL, 'luonghx', 'c4ca4238a0b923820dcc509a6f75849b', NULL, 1, 1, NULL, '2017-08-03 16:33:06', NULL);

SET FOREIGN_KEY_CHECKS = 1;
